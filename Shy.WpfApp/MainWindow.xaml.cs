﻿using Planner.ViewModels;
using Shy.Steps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Shy.WpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        protected override async void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            DataContext = new DesiresViewModel(new Steps.DesireFileRepository());
            await ViewModel.LoadAsync();
        }

        public DesiresViewModel ViewModel => (DesiresViewModel)DataContext;

        private void ShowDesireDetails(Desire desire)
        {
            var desireWindow = new DesireDetailsWindow(desire);            
            desireWindow.Show();
        }

        private DesireViewModel _selectedDesire;
        public DesireViewModel SelectedDesire
        {
            get => _selectedDesire;
            set
            {
                _selectedDesire = value;
                ShowDesireDetails(_selectedDesire.Desire);
            }
        }
    }
}
