﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkTimeCalculator;
using Xunit;

namespace Shy.Tests
{
    public class EffortScheduleTests
    {
        [Fact]
        public void BetweenDates_WithWorkWeekAndOverworkOnSaturday_TotalEffortIs48()
        {
            var defaultWorkingSchedule = new DefaultWorkingSchedule(new DateTime(2010, 1, 1), new DateTime(2011, 1, 1), TimeSpan.FromHours(9));

            var overworkOnSaturday = EffortPeriod.FromStartEndTimeOnDay(new DateTime(2010, 1, 23), TimeSpan.FromHours(11), TimeSpan.FromHours(19));

            var schedule = new EffortSchedule(overworkOnSaturday);

            var workingSchedule = defaultWorkingSchedule.Override(schedule);

            var workingScheduleInWeek = workingSchedule.BetweenDates(new DateTime(2010, 1, 18), new DateTime(2010, 1, 24));
            
            Assert.Equal(48, workingScheduleInWeek.TotalEffort());
        }

        [Fact]
        public void Override_With1OverworkDayOnSaturday_SaturdayIsWorkingDay()
        {
            var defaultWorkingSchedule = new DefaultWorkingSchedule(new DateTime(2010, 1, 1), new DateTime(2011, 1, 1), TimeSpan.FromHours(9));

            var overworkOnSaturday = EffortPeriod.FromStartEndTimeOnDay(new DateTime(2010, 1, 23), TimeSpan.FromHours(11), TimeSpan.FromHours(19));

            var schedule = new EffortSchedule(overworkOnSaturday);
            
            var workingSchedule = defaultWorkingSchedule.Override(schedule);

            var effortOfSaturday = workingSchedule.EffortOfDay(new DateTime(2010, 1, 20));
            Assert.Equal(8, effortOfSaturday);
        }

        [Fact]
        public void Override_With1LeaveDayOnMonday_MondayIsNonWorkingDay()
        {
            var defaultWorkingSchedule = new DefaultWorkingSchedule(new DateTime(2010, 1, 1), new DateTime(2011, 1, 1), TimeSpan.FromHours(9));

            var monday = new DateTime(2010, 1, 18);

            var leaveOnMonday = EffortPeriod.NonWorkingDay(monday);
            var schedule = new EffortSchedule(leaveOnMonday);

            var workingSchedule = defaultWorkingSchedule.Override(schedule);

            var effortOfMonday = workingSchedule.EffortOfDay(monday);
            Assert.Equal(0, effortOfMonday);
        }
    }
}
