﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Shy.Tests
{
    public class PriorityTests
    {
        [Fact]
        public void ctor_WithNanDouble_ThrowsException()
        {
            var exception = Assert.Throws<ArgumentException>(() => new Priority(double.NaN));
            Assert.Equal("Can not be NaN (Parameter 'value')", exception.Message);
        }

        [Theory]
        [InlineData(10, 10, 20)]
        [InlineData(20, 30, 50)]
        [InlineData(0, -50, -50)]
        public void plus_WithValidArguments_SumsPrioririties(double x, double y, double expected)
        {
            var result = new Priority(x) + new Priority(y);
            Assert.Equal(expected, result.Value);
        }
    }
}
