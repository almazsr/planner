﻿using Shy.Steps;
using Shy.Steps.Validation;
using System;
using System.Collections.Generic;
using Xunit;

namespace Shy.Tests
{
    public class WhatToDoHelperTests
    {
        [Fact]
        public void DecideWhatToDoNext_WhenDifferenceBetweenQuestsIsBigAndRemainingEstimateIsSmall_DecideContinueCurrentQuest()
        {
            var whatToDoHelper = new WhatToDoHelper(new SwitchRule[]
            {
                new SwitchRule(10, TimeSpan.FromHours(16), TimeSpan.FromHours(32)),
                new SwitchRule(20, TimeSpan.FromHours(8), TimeSpan.FromHours(16)),
                new SwitchRule(30, TimeSpan.FromHours(4), TimeSpan.FromHours(8)),
                new SwitchRule(40, TimeSpan.FromHours(1), TimeSpan.FromHours(4)),
            });
            var member = new Member();
            var currentQuest = new Quest("Test feature", 20) { RemainingEstimate = Estimate.Exact(Effort.FromHours(3)) };
            member.SwitchTo(currentQuest);
            member.QuestPool.Enqueue(new Quest("Test review", 50));
            member.QuestPool.Enqueue(new Quest("Test bug", 80));
            var decision = whatToDoHelper.DecideWhatToDoNext(member);
            Assert.IsType<ContinueCurrentQuestDecision>(decision);
        }
    }
}
