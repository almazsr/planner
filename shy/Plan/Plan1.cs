﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Shy.Steps
{
    public class Plan : IListRange<object>
    {
        private readonly List<object> _steps;
        private int _lastStepId = 0;

        public Plan()
        {
            _steps = new List<object>();
        }

        public Plan(IEnumerable<object> collection)
        {
            _steps = collection.ToList();
            _lastStepId = _steps.Any() ? _steps.Max(s => s.Id) : 0;
        }

        public IEnumerable<IStep> EnumerateChildren(IStep parent)
        {
            var index = IndexOf(parent);            
            return this.Skip(index + 1).TakeWhile(i => i.IndentLevel > parent.IndentLevel);
        }

        public void RemoveRange(int index, int count)
        {
            _steps.RemoveRange(index, count);
        }

        public void ReplaceAll(IEnumerable<IStep> collection)
        {
            var steps = collection.ToArray();
            _steps.Clear();
            if (steps.GroupBy(s => s.Id).Any(g => g.Count() > 1))
            {
                throw new InvalidOperationException("Non unique ids in collection");
            }
            _steps.AddRange(steps);
        }

        public void AddRange(IEnumerable<IStep> collection)
        {
            var steps = collection.ToArray();
            foreach (var step in steps)
            {
                step.Id = ++_lastStepId;
            }
            _steps.AddRange(steps);
        }

        public void InsertRange(int index, IEnumerable<IStep> collection)
        {
            var steps = collection.ToArray();
            foreach (var step in steps)
            {
                step.Id = ++_lastStepId;
            }
            _steps.InsertRange(index, steps);
        }

        public int IndexOf(IStep item)
        {
            return _steps.IndexOf(item);
        }

        public void Insert(int index, IStep item)
        {
            item.Id = ++_lastStepId;
            _steps.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _steps.RemoveAt(index);
        }

        public object this[int index]
        {
            get => _steps[index];
            set => _steps[index] = value;
        }

        public void Add(object item)
        {
            item.Id = ++_lastStepId;
            _steps.Add(item);
        }

        public void Clear()
        {
            _steps.Clear();
        }

        public bool Contains(IStep item)
        {
            return _steps.Contains(item);
        }

        public void CopyTo(IStep[] array, int arrayIndex)
        {
            _steps.CopyTo(array, arrayIndex);
        }

        public bool Remove(IStep item)
        {
            return _steps.Remove(item);
        }

        public int Count => _steps.Count;
        public bool IsReadOnly => false;

        public IEnumerator<object> GetEnumerator()
        {
            return _steps.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _steps.GetEnumerator();
        }
    }
}
