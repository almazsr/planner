﻿using Shy.Steps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shy
{
    public class SwitchRule
    {
        public SwitchRule(double minWeightDifference, TimeSpan switchTime, TimeSpan finishAndSwitchTime)
        {
            MinWeightDifference = minWeightDifference;
            SwitchTime = switchTime;
            FinishAndSwitchTime = finishAndSwitchTime;
        }

        public TimeSpan SwitchTime { get; set; }
        public TimeSpan FinishAndSwitchTime { get; set; }
        public double MinWeightDifference { get; set; }
    }

    public class WhatToDoDecision
    {

    }

    public class NothingToDoDecision : WhatToDoDecision
    {

    }

    public class StartNewQuestDecision : WhatToDoDecision
    {
        public Quest NextQuest { get; set; }
    }

    public class SwitchToNewQuestDecision : WhatToDoDecision
    {
        public Quest NextQuest { get; set; }

        public TimeSpan SwitchTime { get; set; }
    }

    public class ContinueCurrentQuestDecision : WhatToDoDecision
    {

    }

    public class AssignmentHelper
    {
        public IEnumerable<Member> GetRecommendedMembersForQuest(Quest quest)
        {
            throw new NotImplementedException();
        }

        public void EnqueueQuestForMember(Quest quest, Member member)
        {

        }
    }

    public class Member
    {
        public void SwitchTo(Quest quest)
        {
            CurrentQuest = quest;
        }

        public Quest CurrentQuest { get; private set; }

        public QuestPool QuestPool { get; } = new QuestPool();
    }

    public class QuestPool
    {
        private readonly List<Quest> _quests = new List<Quest>();

        public bool TryDequeue(out Quest quest)
        {
            quest = _quests.OrderByDescending(q => q.Prioriry).FirstOrDefault();
            return quest != null;
        }

        public void Enqueue(Quest quest)
        {
            _quests.Add(quest);
        }
    }

    public class WhatToDoHelper
    {
        private readonly SwitchRule[] _switchRulesOrderedByPriority;

        public WhatToDoHelper(SwitchRule[] switchRules)
        {
            _switchRulesOrderedByPriority = switchRules.OrderByDescending(s => s.MinWeightDifference).ToArray();
        }

        public WhatToDoDecision DecideWhatToDoNext(Member member)
        {
            var currentQuest = member.CurrentQuest;
            var hasNextQuests = member.QuestPool.TryDequeue(out var nextQuest);
            if (currentQuest == null)
            {
                if (!hasNextQuests)
                {
                    return new NothingToDoDecision();
                }
                return new StartNewQuestDecision { NextQuest = nextQuest };
            }
            var weightDifference = nextQuest.Prioriry - currentQuest.Prioriry;

            var switchRule = _switchRulesOrderedByPriority
                .FirstOrDefault(r => weightDifference >= r.MinWeightDifference);

            if (switchRule != null)
            {
                var maxRemainingEffort = currentQuest.RemainingEstimate.Max;
                if (maxRemainingEffort.Time() >= switchRule.FinishAndSwitchTime)
                {
                    return new SwitchToNewQuestDecision { NextQuest = nextQuest, SwitchTime = switchRule.SwitchTime };
                }
            }
            return new ContinueCurrentQuestDecision();
        }
    }
}