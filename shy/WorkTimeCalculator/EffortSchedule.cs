﻿using Shy.Steps;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WorkTimeCalculator
{
    public class EffortSchedule
    {
        public EffortSchedule(params EffortPeriod[] periods) : this (periods.AsEnumerable())
        {
        }

        public EffortSchedule(IEnumerable<EffortPeriod> periods)
        {
            PeriodsByDays = periods.OrderBy(p => p.Date).GroupBy(p => p.Date).ToDictionary(p => p.Key, p => p.ToList());
        }

        protected EffortSchedule(Dictionary<DateTime, List<EffortPeriod>> periodByDays)
        {
            PeriodsByDays = periodByDays;
        }

        public EffortSchedule Override(EffortSchedule schedule)
        {
            if (schedule == null)
            {
                throw new ArgumentNullException(nameof(schedule));
            }

            var unionDays = PeriodsByDays.Keys.Union(schedule.PeriodsByDays.Keys).ToArray();
            var resultPeriodByDays = new Dictionary<DateTime, List<EffortPeriod>>();

            foreach (var day in unionDays)
            {
                if (schedule.PeriodsByDays.ContainsKey(day))
                {
                    resultPeriodByDays.Add(day, schedule.PeriodsByDays[day].Select(p => p.Copy()).ToList());
                }
                else if (PeriodsByDays.ContainsKey(day))
                {
                    resultPeriodByDays.Add(day, PeriodsByDays[day].Select(p => p.Copy()).ToList());
                }
            }

            return new EffortSchedule(resultPeriodByDays);
        }

        protected Dictionary<DateTime, List<EffortPeriod>> PeriodsByDays { get; }

        public IReadOnlyList<EffortPeriod> GetEffortPeriodsOfDay(DateTime day)
        {
            if (PeriodsByDays.ContainsKey(day))
            {
                return PeriodsByDays[day];
            }
            return Array.Empty<EffortPeriod>();
        }

        public Effort EffortOfDay(DateTime day)
        {
            return GetEffortPeriodsOfDay(day).Sum(p => p.Effort);
        }

        public Effort TotalEffort()
        {
            return PeriodsByDays.Values.SelectMany(p => p).Sum(p => p.Effort);
        }

        public EffortSchedule BetweenDates(DateTime startDate, DateTime finishDate)
        {
            if (finishDate < startDate)
            {
                throw new ArgumentException("finishDate must be greater than startDate");
            }

            var periods = new List<EffortPeriod>();

            var day = startDate.Date;
            while (day <= finishDate)
            {
                var startTime = day == startDate.Date ? startDate.TimeOfDay : TimeSpan.Zero;
                var endTime = day == finishDate.Date ? finishDate.TimeOfDay : TimeSpan.FromDays(1);

                var workingPeriods = GetEffortPeriodsOfDay(day)
                    .Select(p => p.Between(startTime, endTime))
                    .Where(p => p != EffortPeriod.NonWorkingDay(day));

                periods.AddRange(workingPeriods);
                day = day.AddDays(1);
            }

            return new EffortSchedule(periods);
        }

        public EffortSchedule StartPlusEffort(DateTime startDate, Effort effort)
        {
            var periods = new List<EffortPeriod>();

            var day = startDate.Date;
            var totalEffort = effort;
            while (totalEffort > Effort.Nothing)
            {
                var startTime = day == startDate.Date ? startDate.TimeOfDay : TimeSpan.Zero;

                var workingPeriods = GetEffortPeriodsOfDay(day)
                    .Select(p => p.StartPlusEffort(startTime, totalEffort))
                    .Where(p => p.Effort > 0);

                totalEffort -= workingPeriods.Sum(p => p.Effort);
                periods.AddRange(workingPeriods);
                day = day.AddDays(1);
            }

            return new EffortSchedule(periods);
        }

        public EffortSchedule EndMinusEffort(Effort effort, DateTime endDate)
        {
            var periods = new List<EffortPeriod>();

            var day = endDate.Date;
            var totalEffort = effort;
            while (totalEffort > Effort.Nothing)
            {
                var endTime = day == endDate.Date ? endDate.TimeOfDay : TimeSpan.FromDays(1);

                var workingPeriods = GetEffortPeriodsOfDay(day)
                    .Select(p => p.EndMinusEffort(endTime, totalEffort))
                    .Where(p => p.Effort > 0);

                totalEffort -= workingPeriods.Sum(p => p.Effort);
                periods.AddRange(workingPeriods);
                day = day.AddDays(1);
            }

            return new EffortSchedule(periods);
        }
    }
}