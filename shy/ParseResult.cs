﻿using System.Collections.Generic;
using System.Linq;

namespace Shy.Steps
{
    public class ParseResult<T, TError>
    {
        public T Data { get; set; }
        public List<TError> Errors { get; set; } = new List<TError>();

        public bool Success => !Errors.Any();
    }
}