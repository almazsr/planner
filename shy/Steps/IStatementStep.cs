﻿namespace Shy.Steps
{
    public interface IStatementStep : IStep
    {
        StepCommand Command { get; }
    }
}