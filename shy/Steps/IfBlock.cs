﻿using System.Collections;
using System.Collections.Generic;

namespace Shy.Steps
{
    public class IfBlock
    {
        public IList<object> ThenPlan { get; } = new List<object>();

        public IList<object> ElsePlan { get; } = new List<object>();
    }
}