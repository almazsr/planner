﻿using System;
using System.Collections.Generic;

namespace Shy.Steps
{
    public class WhenBlock : ICloneable
    {
        public WhenBlock(string body, DateTimeOffset? date, object parent) : this (body, date, parent, Array.Empty<object>())
        {

        }

        private WhenBlock(string body, DateTimeOffset? date, object parent, IEnumerable<object> plan)
        {
            Parent = parent;
            Date = date;
            Body = body;
            Plan = new List<object>();
            foreach (var item in plan)
            {
                if (item is ICloneable cloneable)
                {
                    Plan.Add(cloneable.Clone());
                }
            }
        }

        public string Body { get; }

        public object Parent { get; }

        public IList<object> Plan { get; }

        public DateTimeOffset? Date { get; }

        public WhenBlock Clone()
        {
            return new WhenBlock(Body, Date, Parent, Plan);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}