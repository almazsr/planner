﻿using System;
using System.IO;

namespace Shy.Steps
{
    public class ProjectProvider
    {
        private static Project _project;

        public static Project GetOrReadProject()
        {
            var myDocumentsFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            return _project ?? (_project = Project.ReadFromJson(Path.Combine(myDocumentsFolder, "project.json")));
        }
    }
}