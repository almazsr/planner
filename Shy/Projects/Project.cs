﻿using Newtonsoft.Json;
using System.IO;

namespace Shy.Steps
{
    public class Project
    {
        [JsonIgnore]
        public string FilePath { get; set; }

        public string Name { get; set; }

        public int Id { get; set; }

        public Plan1 Plan { get; set; } = new Plan1();

        public static Project ReadFromJson(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return new Project { FilePath = filePath, Plan = new Plan1 { new DesiresStep(), new RoutineStep() } };
            }
            var json = File.ReadAllText(filePath);
            var result = JsonConvert.DeserializeObject<Project>(json, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
            result.FilePath = filePath;
            return result;
        }

        public void Save()
        {
            File.WriteAllText(FilePath, JsonConvert.SerializeObject(this, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All }));
        }
    }
}