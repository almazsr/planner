﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shy.Steps
{
    public interface IStepParser
    {
        Task<ParseResult<IStep, StepParseError>> ParseStepAsync(string line);
    }
}