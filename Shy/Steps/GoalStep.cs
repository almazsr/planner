﻿namespace Shy.Steps
{


    public class GoalStep : Step, IHasBlock
    {
        public GoalFocus Focus { get; set; }
        public GoalSize Size { get; set; } = GoalSize.Normal;
    }
}