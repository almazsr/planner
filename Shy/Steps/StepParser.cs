﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Shy.Steps.Validation;

namespace Shy.Steps
{
    public class StepParser : IStepParser
    {
        private ITaskNameValidator _taskNameValidator;

        public static StepCommand[] Commands => new[]
        {
            StepCommand.If,
            StepCommand.GoTo,
            StepCommand.When,
            StepCommand.After,
            StepCommand.Goal,
            StepCommand.Then,
            StepCommand.Else,
            StepCommand.Begin
        };

        public StepParser(ITaskNameValidator taskNameValidator)
        {
            _taskNameValidator = taskNameValidator;
        }

        public async Task<ParseResult<IStep, StepParseError>> ParseStepAsync(string text)
        {
            var commandsJoinString = string.Join("|", Commands.Select(c=>c.Name));
            var regexPattern = $@"^(?<label>#\S*)?\s?((?<command>({commandsJoinString}))\s?)?(?<body>.*)";
            var match = Regex.Match(text, regexPattern);
            var result = new ParseResult<IStep, StepParseError>();
            if (!match.Success)
            {
                result.Errors.Add("Can not parse step statement");
            }
            var command = match.Groups["command"].Value;
            if (command != null)
            {
                command = command.Trim();
            }            
            var label = match.Groups["label"].Value;
            var body = match.Groups["body"].Value;

            if (string.IsNullOrEmpty(label))
            {
                label = null;
            }
            if (string.IsNullOrEmpty(body))
            {
                body = null;
            }

            IStep step = null;
            
            if (command == Resources.If)
            {
                step = new IfStep();
            }
            else if (command == Resources.When)
            {
                step = new WhenBlock();
            }
            else if (command == Resources.After)
            {
                var afterStep = new AfterStep();
                var afterRegexPattern = $@"(?<afterLabel>#\S+)";
                var afterMatch = Regex.Match(body, afterRegexPattern);
                if (afterMatch.Success)
                {                    
                    afterStep.AfterLabel = match.Groups["afterLabel"].Value;
                }
                else
                {
                    result.Errors.Add("Can not parse number of after step");
                }
                step = afterStep;
            }
            else if (command == Resources.GoTo)
            {
                var gotoStep = new GoToStep();
                var gotoRegexPattern = $@"(?<destinationLabel>#\S+)";
                var gotoMatch = Regex.Match(body, gotoRegexPattern);
                if (gotoMatch.Success)
                {
                    gotoStep.DestinationLabel = match.Groups["destinationLabel"].Value;
                }
                else
                {
                    result.Errors.Add("Can not parse number of after step");
                }
                step = gotoStep;
            }
            else if (command == Resources.Then)
            {
                step = new ThenStep();
            }
            else if (command == Resources.Else)
            {
                step = new ElseStep();
            }
            else if (command == Resources.Goal)
            {
                step = new GoalStep();
            }
            else if (string.IsNullOrEmpty(command))
            {
                if (!string.IsNullOrEmpty(body))
                {
                    step = new TaskStep();
                    var validateTaskNameTask = Task.Run(() => _taskNameValidator.ValidateTaskName(body));
                    var taskValidationResult = await validateTaskNameTask.ConfigureAwait(false);

                    if (!taskValidationResult.Success)
                    {
                        var stepParseErrors = taskValidationResult.ValidationErrors.Select(s => new StepParseError { Message = s });
                        result.Errors.AddRange(stepParseErrors);
                    }
                }
                else
                {
                    step = new BlockStep();
                }
            }

            if (result.Success)
            {
                step.Body = body;
                step.Label = label;
                result.Data = step;
            }

            return result;
        }
    }
}