﻿namespace Shy.Steps
{
    public class ElseStep : Step, IStatementStep, IHasBlock
    {
        public StepCommand Command => StepCommand.Else;
    }
}