﻿namespace Shy.Steps
{
    public class StepParseError
    {
        public string Message { get; set; }

        public static implicit operator StepParseError(string message)
        {
            return new StepParseError { Message = message };
        }
    }
}