﻿namespace Shy.Steps
{
    public class ThenStep : Step, IStatementStep, IHasBlock
    {
        public StepCommand Command => StepCommand.Then;
    }
}