﻿namespace Shy.Steps
{
    public interface IEstimatedStep : IStep
    {
        Estimate Estimate { get; }
    }
}