﻿using System.Collections.Generic;

namespace Shy.Steps
{
    public interface IStep
    {
        int Id { get; set; }
        string Body { get; set; }
        int IndentLevel { get; set; }
        bool IsDone { get; set; }
        string Label { get; set; }
    }
}