﻿using System;

namespace Shy.Steps
{
    public class DesireStep : Step
    {
        public string Name { get; set; }
        public Uri ImageUri { get; set; }
        public Uri MindMapUri { get; set; }
        public string SuccessWay { get; set; }
        public string FailureWay { get; set; }
    }
}