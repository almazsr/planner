﻿namespace Shy.Steps
{
    public class TaskStep : Step, IEstimatedStep
    {
        public TaskSize Size { get; set; } = TaskSize.Normal;
        public Estimate Estimate { get; set; } = Estimate.Range(Effort.FromHours(4), Effort.FromDays(4));
        public bool IsInProgress { get; set; }
    }
}