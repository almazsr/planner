﻿namespace Shy.Steps
{
    public class GoToStep : Step, IStatementStep
    {
        public StepCommand Command => StepCommand.GoTo;
        public string DestinationLabel { get; set; }
    }
}