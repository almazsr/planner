﻿using System;
using System.Collections.Generic;

namespace Shy.Steps
{
    public class StepCommand : IEquatable<StepCommand>
    {
        public string Name { get; set; }

        public static implicit operator StepCommand(string name)
        {
            return new StepCommand { Name = name };
        }

        public static implicit operator string(StepCommand command)
        {
            return command.Name;
        }

        public static StepCommand None = Resources.Empty;
        public static StepCommand If = Resources.If;
        public static StepCommand GoTo = Resources.GoTo;
        public static StepCommand When = Resources.When;
        public static StepCommand After = Resources.After;
        public static StepCommand Goal = Resources.Goal;
        public static StepCommand Then = Resources.Then;
        public static StepCommand Else = Resources.Else;
        public static StepCommand Begin = Resources.Begin;

        public static bool operator ==(StepCommand left, StepCommand right)
        {
            return EqualityComparer<StepCommand>.Default.Equals(left, right);
        }

        public static bool operator !=(StepCommand left, StepCommand right)
        {
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as StepCommand);
        }

        public bool Equals(StepCommand other)
        {
            return other != null &&
                   Name == other.Name;
        }

        public override int GetHashCode()
        {
            return 539060726 + EqualityComparer<string>.Default.GetHashCode(Name);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}