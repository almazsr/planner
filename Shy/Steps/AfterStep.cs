﻿using System;

namespace Shy.Steps
{
    public class AfterStep : Step, IStatementStep, IHasBlock
    {
        public StepCommand Command => StepCommand.After;
        public string AfterLabel { get; set; }
    }
}