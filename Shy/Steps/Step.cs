﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Shy.Steps
{
    public class Step : IStep
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public bool IsDone { get; set; }
        public int IndentLevel { get; set; }
        public string Label { get; set; }
    }
}