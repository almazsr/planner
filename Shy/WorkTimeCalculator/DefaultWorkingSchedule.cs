﻿using Shy.Steps;
using System;
using System.Collections.Generic;

namespace WorkTimeCalculator
{
    public class DefaultWorkingSchedule : EffortSchedule
    {
        public DefaultWorkingSchedule(DateTime startDate, DateTime endDate, TimeSpan workingDayStartTime) : base(EnumerateWorkingDays(startDate, endDate, workingDayStartTime))
        {

        }

        private static IEnumerable<EffortPeriod> EnumerateWorkingDays(DateTime startDate, DateTime endDate, TimeSpan workingDayStartTime)
        {
            var workingDayEndTime = workingDayStartTime.Add(Effort.Day.Time());
            var date = startDate;
            while (date < endDate)
            {
                var dayOfWeek = (int)date.DayOfWeek;
                var isWeekday = dayOfWeek > 0 && dayOfWeek < 6;
                if (isWeekday)
                {
                    yield return EffortPeriod.FromStartEndTimeOnDay(date, workingDayStartTime, workingDayEndTime);
                }
                date = date.AddDays(1);
            }
        }
    }
}