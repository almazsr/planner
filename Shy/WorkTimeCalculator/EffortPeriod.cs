﻿using Shy.Steps;
using System;
using System.Collections.Generic;

namespace WorkTimeCalculator
{
	public class EffortPeriod : IEquatable<EffortPeriod>
    {
        public static EffortPeriod FromStartEndTimeOnDay(DateTime day, TimeSpan startTime, TimeSpan endTime)
        {
            return new EffortPeriod(day, startTime, endTime);
        }

        public static EffortPeriod FromEffortOnDay(DateTime day, TimeSpan startTime, Effort effort)
        {
            return new EffortPeriod(day, startTime, startTime + effort);
        }

        private EffortPeriod(DateTime date, TimeSpan startTime, TimeSpan endTime)
        {
            Date = date;
            StartTime = startTime;
            EndTime = endTime;
        }

        public static EffortPeriod NonWorkingDay(DateTime date)
        {
            return new EffortPeriod(date, TimeSpan.Zero, TimeSpan.Zero);
        }

        public EffortPeriod Copy()
        {
            return new EffortPeriod(Date, StartTime, EndTime);
        }

		public TimeSpan TimeLength => EndTime - StartTime;

        public Effort Effort => Effort.FromHours((EndTime - StartTime).TotalHours);

		public DateTime Date { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }

        public DateTime StartDate() => Date + StartTime;
        public DateTime EndDate() => Date + EndTime;

        public EffortPeriod After(TimeSpan time)
        {
            return Between(time, TimeSpan.FromDays(1));
        }

        public EffortPeriod Before(TimeSpan time)
        {
            return Between(TimeSpan.Zero, time);
        }

        public EffortPeriod StartPlusEffort(TimeSpan startTime, Effort effort)
        {
            if (startTime < StartTime)
            {
                startTime = StartTime;
            }
            var endTime = startTime.Add(effort.Time());

            var day = TimeSpan.FromDays(1);
            if (endTime >= day)
            {
                endTime = day;
            }

            return Between(startTime, endTime);
        }

        public EffortPeriod EndMinusEffort(TimeSpan endTime, Effort effort)
        {
            if (endTime > EndTime)
            {
                endTime = EndTime;
            }
            var startTime = endTime.Add(-effort.Time());

            if (startTime < TimeSpan.Zero)
            {
                startTime = TimeSpan.Zero;
            }

            return Between(startTime, endTime);
        }

        public EffortPeriod Between(TimeSpan startTime, TimeSpan endTime)
        {
            if (startTime > endTime)
            {
                throw new ArgumentException("endTime must be greater than startTime");
            }
            if (endTime < StartTime || startTime > EndTime)
            {
                return NonWorkingDay(DateTime.MinValue);
            }
            if (startTime < StartTime)
            {
                startTime = StartTime;
            }
            if (endTime > EndTime)
            {
                endTime = EndTime;
            }
            return new EffortPeriod(Date, startTime, endTime);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as EffortPeriod);
        }

        public bool Equals(EffortPeriod other)
        {
            return other != null &&
                   Date == other.Date &&
                   StartTime.Equals(other.StartTime) &&
                   EndTime.Equals(other.EndTime);
        }

        public override int GetHashCode()
        {
            var hashCode = -15780548;
            hashCode = hashCode * -1521134295 + Date.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<TimeSpan>.Default.GetHashCode(StartTime);
            hashCode = hashCode * -1521134295 + EqualityComparer<TimeSpan>.Default.GetHashCode(EndTime);
            return hashCode;
        }

        public static bool operator ==(EffortPeriod left, EffortPeriod right)
        {
            return EqualityComparer<EffortPeriod>.Default.Equals(left, right);
        }

        public static bool operator !=(EffortPeriod left, EffortPeriod right)
        {
            return !(left == right);
        }
    }
}