﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;
using HtmlAgilityPack;
using Shy.Steps;

namespace WorkTimeCalculator
{
	public class TatarstanWorkingSchedule : EffortSchedule
	{
		public static async Task<TatarstanWorkingSchedule> GetAsync(DateTime startDateTime)
		{
			await EnsureInitializedAsync().ConfigureAwait(false);
			return new TatarstanWorkingSchedule(EnumerateWorkingDays(startDateTime));
		}

		private TatarstanWorkingSchedule(IEnumerable<EffortPeriod> periods) : base(periods)
		{

		}

		private struct Month
		{
			public string Name;
			public int[] Holidays;
			public int[] BeforeHolidays;
		}

		private readonly static string[] MonthNames = new[]
		{
			"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь",
			"Декабрь"
		};

		private static readonly Dictionary<int, Month[]> _monthsByYear = new Dictionary<int, Month[]>();
		private static bool _isInitialied;

		private static async Task InitializeAsync(int year)
		{
			var httpClient = new HttpClient();
			var document = new HtmlDocument();
			var response = await httpClient.GetAsync($"https://assistentus.ru/proizvodstvennyj-kalendar-{year}/tatarstan/").ConfigureAwait(false);
			var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
			document.LoadHtml(content);
			var calendarNode = document.DocumentNode.SelectSingleNode("//*[@id=\"content\"]/div[1]/article/div/div");

			var monthTables = calendarNode.SelectNodes("table")
				.Where(e => new[] {"month", "month m3"}.Contains(e.GetAttributeValue("class", null))).ToArray();

			var months = monthTables.Select(m =>
			{
				var cells = m.Descendants("td").ToArray();
				var monthNameCell = cells.FirstOrDefault(e => e.GetAttributeValue("class", null) == "tbbg");
				var holidayCells = cells.Where(e =>
					new[] { "holiday", "holiday tb" }.Contains(e.GetAttributeValue("class", null)));
				var beforeHolidayCells = cells.Where(e =>
					new[] { "before_holiday" }.Contains(e.GetAttributeValue("class", null)));
				return new Month
				{
					Name = monthNameCell.InnerText,
					Holidays = holidayCells.Select(e => e.InnerText).Select(int.Parse).ToArray(),
					BeforeHolidays = beforeHolidayCells.Select(e => e.InnerText.Replace("*", "")).Select(int.Parse).ToArray()
				};
			}).ToArray();
			_monthsByYear.Add(year, months);
		}

		private static async Task EnsureInitializedAsync()
		{
			if (!_isInitialied)
			{
				await InitializeAsync(2020).ConfigureAwait(false);
				_isInitialied = true;
			}
		}

		private static IEnumerable<EffortPeriod> EnumerateWorkingDays(DateTime startDateTime)
		{
			var startTime = startDateTime.TimeOfDay;
			var endTime = startTime+ Effort.Day;
			var endTimeShortened = startTime + Effort.ShortenedDay;

			var date = startDateTime.Date;
			while (true)
			{
				var year = date.Year;
				var months = _monthsByYear[year];
				var monthName = MonthNames[date.Month - 1];

				var month = _monthsByYear[year].FirstOrDefault(m => m.Name == monthName);


				if (month.BeforeHolidays.Contains(date.Day))
				{
					yield return EffortPeriod.FromStartEndTimeOnDay(date, startTime, endTimeShortened);
				}
				if (!month.Holidays.Contains(date.Day))
				{
					yield return EffortPeriod.FromStartEndTimeOnDay(date, startTime, endTime);
				}

				date = date.AddDays(1);
			}
		}
	}
}