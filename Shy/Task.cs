﻿using Shy.Steps;

namespace Shy
{
    public class Quest
    {
        public Quest(string name, double priority)
        {
            Name = name;
            Prioriry = priority;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public double Prioriry { get; set; }

        public Estimate RemainingEstimate { get; set; }
    }
}