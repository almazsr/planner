﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Shy
{
    public class Priority : IEquatable<Priority>
    {
        private const string CannotBeNaNMessage = "Can not be NaN";

        public Priority(double value)
        {
            if (double.IsNaN(value))
            {
                throw new ArgumentException(CannotBeNaNMessage, nameof(value));
            }
            Value = value;
        }

        public double Value { get; }

        public override bool Equals(object obj)
        {
            return Equals(obj as Priority);
        }

        public bool Equals(Priority other)
        {
            return other != null &&
                   Value == other.Value;
        }

        public override int GetHashCode()
        {
            return -1937169414 + Value.GetHashCode();
        }

        public static implicit operator double(Priority x)
        {
            Contract.Assert(x != null);
            return x.Value;
        }

        public static implicit operator Priority(double x)
        {
            return new Priority(x);
        }

        public static bool operator ==(Priority left, Priority right)
        {
            return EqualityComparer<Priority>.Default.Equals(left, right);
        }

        public static bool operator !=(Priority left, Priority right)
        {
            return !(left == right);
        }

        public static bool operator >=(Priority left, Priority right)
        {
            Contract.Assert(left != null);
            Contract.Assert(right != null);
            return left.Value >= right.Value;
        }

        public static bool operator <=(Priority left, Priority right)
        {
            Contract.Assert(left != null);
            Contract.Assert(right != null);
            return left.Value <= right.Value;
        }

        public static bool operator >(Priority left, Priority right)
        {
            Contract.Assert(left != null);
            Contract.Assert(right != null);
            return left.Value > right.Value;
        }

        public static bool operator <(Priority left, Priority right)
        {
            Contract.Assert(left != null);
            Contract.Assert(right != null);
            return left.Value < right.Value;
        }

        public static Priority operator +(Priority left, Priority right)
        {
            Contract.Assert(left != null);
            Contract.Assert(right != null);
            return left.Value + right.Value;
        }
    }
}