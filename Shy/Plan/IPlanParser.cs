﻿using System.IO;
using System.Threading.Tasks;

namespace Shy.Steps
{
    public interface IPlanParser
    {
        Task<ParseResult<Plan1, PlanParseError>> ParsePlanAsync(TextReader textReader);
    }
}