﻿using Shy.Steps.Validation;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Shy.Steps
{
    public class PlanStorage
    {
        public const string PlanFileName = "main.plan";
        private readonly PlanParser _planParser = new PlanParser(new StepParser(new TaskNameValidator()));

        public static string GetFilePath()
        {
            var appDataDirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            appDataDirectoryPath = Path.Combine(appDataDirectoryPath, "Shy");
            if (!Directory.Exists(appDataDirectoryPath))
            {
                Directory.CreateDirectory(appDataDirectoryPath);
            }
            return Path.Combine(appDataDirectoryPath, PlanFileName);
        }

        public async Task WritePlanAsync(Plan1 plan)
        {
            using (var fileWriter = new StreamWriter(GetFilePath()))
            {
                foreach (var step in plan)
                {
                    var tabs = string.Join("", Enumerable.Repeat("\t", step.IndentLevel));
                    var label = !string.IsNullOrEmpty(step.Label) ? $"#{step.Label} " : null;
                    var command = string.Empty;
                    if (step is IStatementStep statement)
                    {
                        command = statement.Command;
                    }
                    await fileWriter.WriteLineAsync($"{tabs}{label}{command}{step.Body}");
                }
            }
        }

        public async Task<Plan1> ReadPlanAsync()
        {
            var filePath = GetFilePath();
            if (!File.Exists(filePath))
            {
                return new Plan1();
            }
            using (var fileReader = new StreamReader(filePath))
            {
                var result = await _planParser.ParsePlanAsync(fileReader);
                if (!result.Success)
                {
                    throw new InvalidOperationException("Can not parse plan");
                }
                return result.Data;
            }
        }
    }
}