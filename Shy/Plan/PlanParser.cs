﻿using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Shy.Steps
{
    public class PlanParser : IPlanParser
    {
        private readonly IStepParser _stepParser;

        public PlanParser(IStepParser stepParser)
        {
            _stepParser = stepParser;
        }

        public async Task<ParseResult<Plan1, PlanParseError>> ParsePlanAsync(TextReader textReader)
        {
            var result = new ParseResult<Plan1, PlanParseError>();
            string line;
            int lineNumber = 0;
            var plan = new Plan1();
            IStep previousStep = null;
            while ((line = await textReader.ReadLineAsync()) != null)
            {
                var indentLevel = line.TakeWhile(c => c == '\t').Count();
                line = line.Substring(indentLevel);
                var stepParseResult = await _stepParser.ParseStepAsync(line);                
                if (stepParseResult.Success)
                {
                    var step = stepParseResult.Data;
                    step.IndentLevel = indentLevel;
                    plan.Add(step);
                    previousStep = step;
                }
                else
                {
                    var parserErrors = stepParseResult.Errors.Select(se => new PlanParseError { LineNumber = lineNumber, Message = se.Message });

                    result.Errors.AddRange(parserErrors);
                    previousStep = null;
                }

                lineNumber++;
            }
            if (result.Success)
            {
                result.Data = plan;
            }
            return result;
        }
    }
}