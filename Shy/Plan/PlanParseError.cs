﻿namespace Shy.Steps
{
    public class PlanParseError
    {
        public int LineNumber { get; set; }
        public string Message { get; set; }
    }
}