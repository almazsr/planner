﻿using System;

namespace Shy.Steps
{
    public class GoalSize : IComparable<GoalSize>
    {
        private GoalSize(string val)
        {
            Value = val;
        }

        public string Value { get; private set; }

        public static implicit operator string(GoalSize val)
        {
            return val.Value;
        }

        public static GoalSize Small = new GoalSize(Resources.Small);
        public static GoalSize Normal = new GoalSize(Resources.Normal);
        public static GoalSize Large = new GoalSize(Resources.Large);

        public static readonly GoalSize[] Order = new GoalSize[] { Small, Normal, Large };

        public int CompareTo(GoalSize other)
        {
            var thisIndex = Array.IndexOf(Order, this);
            var otherIndex = Array.IndexOf(Order, other);
            return thisIndex.CompareTo(otherIndex);
        }

        public override string ToString()
        {
            return Value;
        }
    }
}