﻿namespace Shy.Steps
{
    public enum GoalFocus
    {
        None = 0,
        Duration = 1,
        Result = 2,
        Deadline = 3
    }
}