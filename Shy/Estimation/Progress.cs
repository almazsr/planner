﻿using System;
using System.Collections.Generic;

namespace Shy.Steps
{
    public class Progress : IEquatable<Progress>
    {
        public static readonly Progress Unknown = new Progress(Estimate.Unknown, Effort.Unknown);

        public static Progress Zero(Estimate totalEstimate)
        {
            return totalEstimate != Estimate.Unknown ? new Progress(totalEstimate, Effort.Nothing) : Unknown;
        }

        public static Progress Working(Effort effort, Estimate totalEstimate)
        {
            return totalEstimate != Estimate.Nothing && totalEstimate != Estimate.Unknown && effort != Effort.Unknown ? new Progress(totalEstimate, effort) : Unknown;             
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Progress);
        }

        public bool Equals(Progress other)
        {
            return other != null &&
                   EqualityComparer<Estimate>.Default.Equals(Estimate, other.Estimate) &&
                   EqualityComparer<Effort>.Default.Equals(Done, other.Done);
        }

        public override int GetHashCode()
        {
            var hashCode = 1869290994;
            hashCode = hashCode * -1521134295 + EqualityComparer<Estimate>.Default.GetHashCode(Estimate);
            hashCode = hashCode * -1521134295 + EqualityComparer<Effort>.Default.GetHashCode(Done);
            return hashCode;
        }

        private Progress(Estimate estimate, Effort doneEffort)
        {
            Estimate = estimate;
            Done = doneEffort;
        }

        public Estimate Estimate { get; set; }

        public Effort Done { get; set; }

        public double Average => Estimate.Average / Done;

        public double Min => Estimate.Min / Done;

        public double Max => Estimate.Max / Done;

        public static bool operator ==(Progress left, Progress right)
        {
            return EqualityComparer<Progress>.Default.Equals(left, right);
        }

        public static bool operator !=(Progress left, Progress right)
        {
            return !(left == right);
        }
    }
}