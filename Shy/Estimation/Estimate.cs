﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Shy.Steps
{
    public sealed class Estimate : IEquatable<Estimate>
    {
        public static double DefaultDispersion = 0.5;

        public static readonly Estimate Unknown = Exact(Effort.Unknown);
        public static readonly Estimate Nothing = Exact(Effort.Nothing);

        private Estimate(Effort min, Effort max)
        {
            Min = min;
            Max = max;
        }

        public bool IsUnknown => Min < 0 || Max < 0;

        public static Estimate Range(Effort min, Effort max)
        {
            return new Estimate(min, max);
        }
        public static Estimate Exact(Effort value)
        {
            return new Estimate(value, value);
        }
        public static Estimate Aproximate(Effort value, double dispersion = -1)
        {
            if (dispersion < 0)
            {
                dispersion = DefaultDispersion;
            }
            return new Estimate(value * (1 - dispersion), value * (dispersion + 1));
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Estimate);
        }

        public bool Equals(Estimate other)
        {
            return other != null &&
                   EqualityComparer<Effort>.Default.Equals(Min, other.Min) &&
                   EqualityComparer<Effort>.Default.Equals(Max, other.Max);
        }

        public override int GetHashCode()
        {
            var hashCode = 1537547080;
            hashCode = hashCode * -1521134295 + EqualityComparer<Effort>.Default.GetHashCode(Min);
            hashCode = hashCode * -1521134295 + EqualityComparer<Effort>.Default.GetHashCode(Max);
            return hashCode;
        }

        public Effort Min { get; }
        public Effort Max { get; }

        public Effort Average => (Max + Min) / 2;

        public static bool operator ==(Estimate left, Estimate right)
        {
            return EqualityComparer<Estimate>.Default.Equals(left, right);
        }

        public static bool operator !=(Estimate left, Estimate right)
        {
            return !(left == right);
        }

        public static Estimate operator +(Estimate left, Estimate right)
        {
            return (left != Unknown && right != Unknown) ? Range(left.Min + right.Min, left.Max + right.Max) : Unknown;
        }
        public static Estimate operator +(Estimate left, Effort right)
        {
            return (left != Unknown && right != Effort.Unknown) ? Range(left.Min + right, left.Max + right) : Unknown;
        }
        public static Estimate operator -(Estimate left, Effort right)
        {
            return (left != Unknown && right != Effort.Unknown) ? Range(left.Min - right, left.Max - right) : Unknown;
        }

        public static Estimate operator *(Estimate left, Estimate right)
        {
            return (left != Unknown && right != Unknown) ? Range(left.Min * right.Min, left.Max * right.Max) : Unknown;
        }

        public static Estimate Parse(string input)
        {
            var splits = input.Split('-').Select(s => s.Trim()).ToArray();
            if (splits.Length == 0 || splits.Length > 2)
            {
                throw new ArgumentException(nameof(input));
            }

            var minEffort = Effort.Parse(splits[0]);
            if (splits.Length == 1)
            {
                return Exact(minEffort);
            }
            var maxEffort = Effort.Parse(splits[1]);
            return Range(minEffort, maxEffort);
        }

        public override string ToString()
        {
            if (Min == Max)
            {
                return $"{Min}";
            }
            return $"{Min} - {Max}";
        }
    }
}