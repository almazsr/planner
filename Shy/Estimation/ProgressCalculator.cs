﻿using System.Collections.Generic;
using System.Linq;

namespace Shy.Steps
{
    public class ProgressCalculator : IProgressCalculator
    {
        private Estimate Sum(IEnumerable<Estimate> estimates) => estimates.Aggregate(Estimate.Nothing, (sum, e) => sum += e);

        public Progress CalculateProgress(IList<IStep> steps)
        {
            var estimatedSteps = steps.OfType<IEstimatedStep>().ToArray();
            var doneEstimatedSteps = estimatedSteps.Where(e => e.IsDone);
            var doneEstimate = Sum(doneEstimatedSteps.Select(s => s.Estimate));
            var totalEstimate = Sum(estimatedSteps.Select(s => s.Estimate));

            return Progress.Working(doneEstimate.Average, totalEstimate);
        }
    }
}