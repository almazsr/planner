﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shy.Steps
{
    public class Effort : IEquatable<Effort>
    {
        public const int HoursPerDay = 8;
        public const int DaysPerWeek = 5;
        public const int WeeksPerMonth = 4;
        public const int MonthsPerQuarter = 3;
        public const int QuarterPerYear = 4;

        public static readonly Effort Unknown = FromHours(-1);
        public static readonly Effort Nothing = FromHours(0);
        public static readonly Effort Day = FromHours(HoursPerDay);
        public static readonly Effort ShortenedDay = FromHours(HoursPerDay - 1);

        public static Effort FromHours(double hours)
        {
            return new Effort { Hours = hours };
        }
        public static Effort FromDays(double days)
        {
            return days * HoursPerDay;
        }
        public static Effort FromWeeks(double weeks)
        {
            return weeks * DaysPerWeek * HoursPerDay;
        }
        public static Effort FromMonths(double months)
        {
            return months * WeeksPerMonth * DaysPerWeek * HoursPerDay;
        }
        public static Effort FromQuarters(double quarters)
        {
            return quarters * MonthsPerQuarter * WeeksPerMonth * DaysPerWeek * HoursPerDay;
        }
        public static Effort FromYears(double years)
        {
            return years * QuarterPerYear * MonthsPerQuarter * WeeksPerMonth * DaysPerWeek * HoursPerDay;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Effort);
        }

        public bool Equals(Effort other)
        {
            return other != null &&
                   Hours == other.Hours;
        }

        public override int GetHashCode()
        {
            return -1713926412 + Hours.GetHashCode();
        }

        public double Hours { get; set; }

        public double Days => Hours / HoursPerDay;

        public double Weeks => Days / DaysPerWeek;

        public double Months => Weeks / WeeksPerMonth;

        public double Quarters => Months / MonthsPerQuarter;

        public double Years => Quarters / QuarterPerYear;

        public TimeSpan Time() => TimeSpan.FromHours(Hours);

        public static implicit operator double(Effort x)
        {
            return x.Hours;
        }

        public static implicit operator Effort(double x)
        {
            return x >= 0 ? new Effort { Hours = x } : Unknown;
        }

        public static bool operator ==(Effort left, Effort right)
        {
            return EqualityComparer<Effort>.Default.Equals(left, right);
        }

        public static bool operator !=(Effort left, Effort right)
        {
            return !(left == right);
        }

        public static Effort operator *(Effort left, Effort right)
        {
            if (left == Unknown || right == Unknown)
            {
                return Unknown;
            }
            return FromHours(left.Hours * right.Hours);
        }

        public static TimeSpan operator +(TimeSpan left, Effort right)
        {
            if (right == Unknown)
            {
                return TimeSpan.MaxValue;
            }
            return left + right.Time();
        }

        public static Effort operator +(Effort left, Effort right)
        {
            if (left == Unknown || right == Unknown)
            {
                return Unknown;
            }
            return FromHours(left.Hours + right.Hours);
        }

        public static Effort operator -(Effort left, Effort right)
        {
            if (left == Unknown || right == Unknown)
            {
                return Unknown;
            }
            return FromHours(left.Hours - right.Hours);
        }

        public static Effort operator /(Effort left, Effort right)
        {
            if (left == Unknown || right == Unknown)
            {
                return Unknown;
            }
            return FromHours(left.Hours / right.Hours);
        }

        private static Dictionary<double, string[]> _unitsByEffort = new Dictionary<double, string[]>
        {
            [0.5] = new[] { "m", "minutes", "min" },
            [1] = new[] { "h", "hours", "hrs" },
            [HoursPerDay] = new[] { "d", "days" },
            [HoursPerDay * DaysPerWeek] = new[] { "w", "weeks" },
            [HoursPerDay * DaysPerWeek * WeeksPerMonth] = new[] { "m", "months" },
            [HoursPerDay * DaysPerWeek * WeeksPerMonth * MonthsPerQuarter] = new[] { "q", "quarters" },
            [HoursPerDay * DaysPerWeek * WeeksPerMonth * MonthsPerQuarter * QuarterPerYear] = new[] { "y", "years" }
        };

        public static Effort Parse(string input)
        {
            foreach (var units in _unitsByEffort)
            {
                var unit = units.Value.FirstOrDefault(u => input.Contains(u));
                if (unit != null)
                {
                    input = input.Replace(unit, string.Empty);
                    var hours = double.Parse(input);
                    return FromHours(hours * units.Key);
                }
            }
            throw new ArgumentException("Invalid input", nameof(input));
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            var restHours = Hours;
            foreach (var units in _unitsByEffort.OrderByDescending(e => e.Key))
            {
                var val = restHours / units.Key;
                if (val >= 1)
                {
                    stringBuilder.Append($"{(int)(restHours / units.Key)}{units.Value[0]} ");
                    restHours %= units.Key;
                }
            }
            return stringBuilder.ToString().TrimEnd();
        }
    }
}