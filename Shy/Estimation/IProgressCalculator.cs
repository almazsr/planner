﻿using System.Collections.Generic;

namespace Shy.Steps
{
    public interface IProgressCalculator
    {
        Progress CalculateProgress(IList<IStep> steps);
    }
}