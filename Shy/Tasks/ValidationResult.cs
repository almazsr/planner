﻿using System.Collections.Generic;
using System.Linq;

namespace Shy.Steps.Validation
{
    public class ValidationResult
    {
        public bool Success => !ValidationErrors.Any();
        public List<string> ValidationErrors { get; } = new List<string>();
    }
}
