﻿namespace Shy.Steps.Validation
{
    public interface ITaskNameValidator
    {
        ValidationResult ValidateTaskName(string input);
    }
}