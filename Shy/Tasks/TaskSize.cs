﻿using System;

namespace Shy.Steps
{
    public class TaskSize : IComparable<TaskSize>
    {
        private TaskSize(string val)
        {
            Value = val;
        }

        public string Value { get; private set; }

        public static implicit operator string(TaskSize val)
        {
            return val.Value;
        }

        public static TaskSize Small = new TaskSize(Resources.Small);

        public static TaskSize Normal = new TaskSize(Resources.Normal);

        public static readonly TaskSize[] Order = new TaskSize[] { Small, Normal };

        public int CompareTo(TaskSize other)
        {
            var thisIndex = Array.IndexOf(Order, this);
            var otherIndex = Array.IndexOf(Order, other);
            return thisIndex.CompareTo(otherIndex);
        }

        public override string ToString()
        {
            return Value;
        }
    }
}