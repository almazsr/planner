﻿using System;
using System.Collections.Generic;
using System.Linq;
using LingvoNET;

namespace Shy.Steps.Validation
{
    public partial class TaskNameValidator : ITaskNameValidator
	{
		public const int MaxWordsCount = 30;
		public const int MinWordsCount = 2;
		public const int MaxValidVerbsCount = 2;

        private readonly HashSet<string> _whiteListVerbs;

        public TaskNameValidator() : this(new string[] { })
        {

        }

        public TaskNameValidator(string[] whiteListVerbs)
		{
            _whiteListVerbs = new HashSet<string>(whiteListVerbs);
        }

        public bool IsVerbValid(string verbString)
        {
            if (_whiteListVerbs.Contains(verbString.ToLower()))
            {
                return true;
            }
            var verb = Verbs.FindOne(verbString);
            if (verb != null)
            {
                return verb.Aspect == VerbAspect.Perfect || verb.Aspect == VerbAspect.PerfectImperfect;
            }
            return false;
        }

        public ValidationResult ValidateTaskName(string name)
		{
            var result = new ValidationResult();
			if(string.IsNullOrWhiteSpace(name))
			{
                result.ValidationErrors.Add("Name cannot be empty");
            }
            if (!result.Success)
            {
                return result;
            }

            var words = name.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

			if(words.Length < MinWordsCount)
			{
                result.ValidationErrors.Add($"Words count should be greater or equal than {MinWordsCount}");
			}

			if(words.Length > MaxWordsCount)
			{
                result.ValidationErrors.Add($"Words count should be less or equal than {MaxWordsCount}");
			}

            if (!result.Success)
            {
                return result;
            }

			var firstWord = words[0];

			if(!IsVerbValid(firstWord))
			{
                result.ValidationErrors.Add("First word should be a valid verb");
            }
            if (!result.Success)
            {
                return result;
            }

            var validVerbsCount = words.Skip(1).Count(IsVerbValid) + 1;

			if(validVerbsCount > MaxValidVerbsCount)
			{
                result.ValidationErrors.Add($"Verbs count should be less or equal than {MaxValidVerbsCount}");
			}

			return result;
        }
    }
}
