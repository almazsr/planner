using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Components;
using Shy.Steps;
using Shy.Steps.Validation;
using Planner.ViewModels;
using Planner.Models;
using System;
using Planner.Pages;

namespace Planner
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(x => x.EnableEndpointRouting = false)
                   .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_3_0);

            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<ITaskNameValidator, TaskNameValidator>();
            services.AddSingleton<IStepParser, StepParser>();
            services.AddSingleton<IProgressCalculator, ProgressCalculator>();
            services.AddSingleton<IPlanParser, PlanParser>();
            services.AddSingleton<ITaskSizeConverter, TaskSizeConverter>();
            services.AddSingleton<Services>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, Services services)
        {
            app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseMvcWithDefaultRoute();
            app.UseMvc();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
