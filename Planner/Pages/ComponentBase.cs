﻿using Microsoft.AspNetCore.Components;
using Planner.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Planner.Pages
{
    public class ComponentBase<TViewModel> : LayoutComponentBase where TViewModel : BaseViewModel
    {
        [Parameter]
        public TViewModel ViewModel { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        protected override void OnInitialized()
        {
            ViewModel.SetNavigationManager(NavigationManager);
            ViewModel.PropertyChanged += OnPropertyChanged;
        }

        public void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            StateHasChanged();
        }
    }
}
