﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Html;
using Planner.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Shy.Steps;

namespace Planner.Pages
{
    public partial class IndexComponent : ComponentBase
    {
        public Plan1 DefaultPlan { get; set; }

        public IndexComponent()
        {
            DefaultPlan = new Plan1();
        }

        //public Dictionary<string, Type> AvailableComponentTypes = new Dictionary<string, Type>
        //{
        //    ["owner"] = typeof(OwnerComponent),
        //    ["status"] = typeof(StatusComponent),
        //    ["focus"] = typeof(ActivityFocusComponent),
        //    ["time size"] = typeof(TimeSize),
        //    ["failure way"] = typeof(FailureWayComponent),
        //    ["success way"] = typeof(SuccessWayComponent),
        //    ["image"] = typeof(ImageComponent),
        //    ["mind map"] = typeof(MindMapComponent),
        //    ["review schedule"] = typeof(ReviewScheduleComponent),
        //    ["plan"] = typeof(PlanComponent1)
        //};

        public string Input { get; set; }
    }
}
