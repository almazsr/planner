﻿using Planner.Models;
using Shy.Steps;
using System;

namespace Planner.ViewModels
{
    public class DesireEditPageViewModel : ComplexStepEditViewModelBase<DesireStep>
    {
        public DesireEditPageViewModel(ProjectViewModel project, DesireStep desire) : base(project, desire)
        {

        }

        public string Name
        {
            get => Step.Name;
            set => Step.Name = value;
        }
        public Uri ImageUri => Step.ImageUri;
        public Uri MindMapUri => Step.MindMapUri;

        public string SuccessWay
        {
            get => Step.SuccessWay;
            set => Step.SuccessWay = value;
        }

        public string FailureWay
        {
            get => Step.FailureWay;
            set => Step.FailureWay = value;
        }
    }
}
