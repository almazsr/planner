﻿using Shy.Steps;

namespace Planner.Models
{
    public class BlockStepViewModel : StepViewModelBase<BlockStep>
    {
        public BlockStepViewModel(PlanViewModel plan, BlockStep step) : base(plan, step)
        {

        }
    }
}
