﻿using Shy.Steps;

namespace Planner.Models
{
    public interface ITaskSizeConverter
    {
        Estimate TaskSizeToEstimate(TaskSize taskSize); 

        TaskSize EstimateToTaskSize(Estimate estimate);
    }
}
