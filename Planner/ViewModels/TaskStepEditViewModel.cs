﻿using Shy.Steps;
using System;
using System.Linq;

namespace Planner.Models
{
    public class TaskStepEditViewModel : StepEditViewModelBase<TaskStep>
    {
        public TaskStepEditViewModel(PlanViewModel plan, TaskStep step) : base(plan, step)
        {
            Estimate = new EstimateEditViewModel(step.Estimate);
            TaskSize = new TaskSizeEditViewModel(step.Size);
            Estimate.ModelChanged += OnEstimateChanged;
            TaskSize.ModelChanged += OnTaskSizeChanged;
        }

        public EstimateEditViewModel Estimate { get; set; }

        public TaskSizeEditViewModel TaskSize { get; set; }

        protected bool IsInProgress
        {
            get => Step.IsInProgress;
            set
            {
                Step.IsInProgress = value;
                Save();
            }
        }

        public void Start()
        {
            IsInProgress = true;
        }

        public void Stop()
        {
            IsInProgress = false;
        }

        protected virtual void OnTaskSizeChanged(object sender, EventArgs e)
        {
            Step.Size = TaskSize.Model;
        }

        protected virtual void OnEstimateChanged(object sender, EventArgs e)
        {
            Step.Estimate = Estimate.Model;
        }

        public void Dispose()
        {
            Estimate.ModelChanged -= OnEstimateChanged;
            TaskSize.ModelChanged -= OnTaskSizeChanged;
        }
    }
}
