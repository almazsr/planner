﻿namespace Planner.Models
{
    public class FirstStepViewModel : NewStepViewModel
    {
        public FirstStepViewModel(PlanViewModel plan, int indentLevel) : base(plan, indentLevel)
        {

        }

        public bool IsInEditMode { get; set; }

        public override void Click()
        {
            Plan.EndEditStep();
            Plan.EndCreateStep();
            Plan.BeginCreateStep(this);
            IsInEditMode = !IsInEditMode;
        }

        public override void Commit()
        {
            IsInEditMode = !IsInEditMode;
            Plan.Replace(this, this);
        }
    }
}
