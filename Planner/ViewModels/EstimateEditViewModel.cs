﻿using Planner.ViewModels;
using Shy.Steps;
using System;
using System.Linq;

namespace Planner.Models
{
    public class EstimateEditViewModel : BaseViewModel
    {
        public EstimateEditViewModel(Estimate estimate)
        {
            _estimate = estimate;
        }

        public bool IsParsed { get; private set; }

        public string ErrorMessage { get; private set; }

        public bool IsParsedSuccessfully => IsParsed && string.IsNullOrEmpty(ErrorMessage);

        private Estimate _estimate;
        public Estimate Model
        {
            get => _estimate;
            set => _estimate = value;
        }

        private void UpdateModel(Estimate value)
        {
            _estimate = value;
            OnModelChanged(this, EventArgs.Empty);
        }

        private void SetEstimate(string value)
        {
            try
            {
                IsParsed = false;
                ErrorMessage = null;
                var estimate = Estimate.Parse(value);
                UpdateModel(estimate);
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            finally
            {
                IsParsed = true;
            }
        }

        public string EstimateText
        {
            get => _estimate?.ToString();
            set => SetEstimate(value);
        }

        public event EventHandler ModelChanged;

        protected virtual void OnModelChanged(object sender, EventArgs e)
        {
            ModelChanged?.Invoke(sender, e);
        }
    }
}
