﻿using Planner.ViewModels;
using Shy.Steps;
using System;

namespace Planner.Models
{
    public class GoalSizeEditViewModel : BaseViewModel
    {
        private GoalSize _goalSize;

        public GoalSizeEditViewModel(GoalSize model)
        {
            _goalSize = model;
        }

        public GoalSize[] Sizes => GoalSize.Order;

        public GoalSize Model
        {
            get => _goalSize;
            set => _goalSize = value;
        }

        public void SelectSize(GoalSize size)
        {
            _goalSize = size;
            OnModelChanged(null, EventArgs.Empty);
        }

        public event EventHandler ModelChanged;

        protected virtual void OnModelChanged(object sender, EventArgs e)
        {
            ModelChanged?.Invoke(sender, e);
        }
    }
}
