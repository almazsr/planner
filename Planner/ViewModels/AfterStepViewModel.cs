﻿using Shy.Steps;

namespace Planner.Models
{
    public class AfterStepViewModel : StepViewModelBase<AfterStep>
    {
        public AfterStepViewModel(PlanViewModel plan, AfterStep model) : base(plan, model)
        {

        }

        public string Command => StepCommand.After;

        public string AfterLabel => Step.AfterLabel;
    }
}
