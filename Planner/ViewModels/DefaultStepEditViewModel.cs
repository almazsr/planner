﻿using Shy.Steps;

namespace Planner.Models
{
    public class DefaultStepEditViewModel : StepEditViewModelBase<IStep>
    {
        public DefaultStepEditViewModel(PlanViewModel plan, IStep model) : base(plan, model)
        {
        }

        public override void Click()
        {
            Commit();
        }
    }
}
