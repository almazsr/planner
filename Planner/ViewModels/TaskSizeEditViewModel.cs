﻿using Planner.ViewModels;
using Shy.Steps;
using System;

namespace Planner.Models
{
    public class TaskSizeEditViewModel : BaseViewModel
    {
        private TaskSize _taskSize;

        public TaskSizeEditViewModel(TaskSize model)
        {
            _taskSize = model;
        }

        public TaskSize[] Sizes => TaskSize.Order;

        public TaskSize Model
        {
            get => _taskSize;
            set => _taskSize = value;
        }

        public void SelectSize(TaskSize size)
        {
            _taskSize = size;
            OnModelChanged(null, EventArgs.Empty);
        }

        public event EventHandler ModelChanged;

        protected virtual void OnModelChanged(object sender, EventArgs e)
        {
            ModelChanged?.Invoke(sender, e);
        }
    }
}
