﻿using Shy.Steps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Planner.ViewModels
{

    public class DesireViewModel : BaseViewModel
    {
        private readonly DesireStep _desire;

        private readonly Random _rand = new Random(DateTime.Now.Millisecond);

        public int GetNextId()
        {
            Thread.Sleep(100);
            return _rand.Next(0, 200);
        }

        public int Id => _desire.Id;

        public ProjectViewModel Project { get; }        

        public DesireViewModel(ProjectViewModel project, DesireStep desire)
        {
            Project = project;
            _desire = desire;
            var desireSubPlan = Project.Model.Plan.EnumerateChildren(_desire).ToArray();
            Progress = new DesireProgressViewModel(desireSubPlan);
        }

        public DesireProgressViewModel Progress { get; }

        public string Name => _desire.Name;
        public Uri ImageUri => _desire.ImageUri;
        public Uri MindMapUri => _desire.MindMapUri;

        public void GoToPage()
        {
            NavigationManager.NavigateTo($"/projects/{Project.Id}/desires/{Id}");
        }
    }
}
