﻿using Shy.Steps;

namespace Planner.Models
{
    public class IfStepViewModel : StepViewModelBase<IfStep>
    {
        public IfStepViewModel(PlanViewModel plan, IfStep model) : base(plan, model)
        {

        }

        public string Command => Step.Command;
    }
}
