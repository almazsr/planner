﻿using Planner.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Planner.Models
{
    public class PlanItemViewModel : BaseViewModel, IPlanItemViewModel
    {
        public PlanItemViewModel(PlanViewModel plan, int indentLevel)
        {
            Plan = plan;
            IndentLevel = indentLevel;
        }

        protected PlanViewModel Plan { get; }
        public int IndentLevel { get; }

        public virtual void Click()
        {

        }

        public void ToggleChildrenVisibility()
        {
            IsCollapsed = !IsCollapsed;
            Plan.ToggleChidlrenVisibility(this);
        }

        public bool HasChidlren()
        {
            return Plan.EnumerateChildren(this).Any();
        }

        public IEnumerable<PlanItemViewModel> Children()
        {
            return Plan.EnumerateChildren(this);
        }

        public bool IsCollapsed { get; set; }
        public bool IsVisible { get; set; } = true;
    }
}
