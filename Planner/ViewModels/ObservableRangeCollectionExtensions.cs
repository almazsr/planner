﻿using Shy.Steps;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Planner.ViewModels
{
    public static class ObservableRangeCollectionExtensions
    {
        public static void SafeInsert<T>(this IListRange<T> items, int index, T item)
        {
            if (index < 0)
            {
                index = 0;
            }
            if (index >= items.Count)
            {
                items.Add(item);
            }
            else
            {
                items.Insert(index, item);
            }
        }

        public static void SafeInsertRange<T>(this IListRange<T> items, int index, IEnumerable<T> newItems)
        {
            if (index < 0)
            {
                index = 0;
            }
            if (index >= items.Count)
            {
                items.AddRange(newItems);
            }
            else
            {
                items.InsertRange(index, newItems);
            }
        }

        public static void SafeRemoveAt<T>(this IListRange<T> items, int index)
        {
            if (index >= 0 && index < items.Count)
            {
                items.RemoveAt(index);
            }
        }

        public static void SafeRemoveRangeAt<T>(this IListRange<T> items, int index, int count)
        {
            if (index >= 0 && index < items.Count && count > 0)
            {
                items.RemoveRange(index, count);
            }
        }

        public static void SafeReplace<T>(this IListRange<T> items, T oldItem, T newItem)
        {
            var index = items.IndexOf(oldItem);
            SafeReplaceAt(items, index, newItem);
        }

        public static void SafeReplaceAt<T>(this IListRange<T> items, int index, T item)
        {
            items.SafeRemoveAt(index);
            items.SafeInsert(index, item);
        }

        public static void SafeReplaceRange<T>(this IListRange<T> items, T oldItem, IEnumerable<T> newItems)
        {
            var index = items.IndexOf(oldItem);            
            SafeReplaceRangeAt(items, index, newItems);
        }

        public static void SafeReplaceRangeAt<T>(this IListRange<T> items, int index, int count, IEnumerable<T> newItems)
        {
            items.SafeRemoveRangeAt(index, count);
            items.SafeInsertRange(index, newItems);
        }

        public static void SafeReplaceRangeAt<T>(this IListRange<T> items, int index, IEnumerable<T> newItems)
        {
            items.SafeRemoveAt(index);
            items.SafeInsertRange(index, newItems);
        }
    }
}
