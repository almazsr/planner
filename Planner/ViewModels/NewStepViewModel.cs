﻿using Shy.Steps;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Planner.Models
{
    public class NewStepViewModel : PlanItemViewModel
    {
        public NewStepViewModel(PlanViewModel plan, int indentLevel) : base(plan, indentLevel)
        {

        }

        public string Text { get; set; }
        public IList<StepParseError> ValidationErrors { get; set; } = new List<StepParseError>();

        public async Task SubmitNewStepAsync()
        {
            ValidationErrors.Clear();
            var result = await Services.StepParser.ParseStepAsync(Text);
            if (result.Success)
            {
                var step = result.Data;                   
                step.IndentLevel = IndentLevel;
                if (step.Label == null || !Services.LabelsCache.ContainsLabel(step.Label))
                {
                    if (step.Label != null)
                    {
                        Services.LabelsCache.Add(step.Label, step);
                    }
                    Plan.InsertNewStep(this, step);
                }
                else
                {
                    // TODO: Replace with exceptions
                    ValidationErrors.Add(new StepParseError { Message = "Duplicate label" });
                }
            }
            else
            {
                ValidationErrors = result.Errors;
            }
            RaisePropertyChanged();
        }

        public virtual void Cancel()
        {
            Plan.EndCreateStep();
        }

        public virtual void Commit()
        {
            Plan.RemoveItem(this);
        }
    }
}
