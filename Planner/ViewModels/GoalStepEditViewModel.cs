﻿using Planner.ViewModels;
using Shy.Steps;
using System;

namespace Planner.Models
{
    public class GoalStepEditViewModel : ComplexStepEditViewModelBase<GoalStep>
    {
        public GoalStepEditViewModel(ProjectViewModel project, GoalStep step) : base(project, step)
        {
            GoalSize = new GoalSizeEditViewModel(step.Size);
            GoalSize.ModelChanged += OnGoalSizeChanged;
        }

        private void OnGoalSizeChanged(object sender, EventArgs e)
        {
            Step.Size = GoalSize.Model;
        }

        public GoalSizeEditViewModel GoalSize { get; set; }

        public string Body => Step.Body;
        public string Label => Step.Label;
        public bool IsDone => Step.IsDone;
    }
}
