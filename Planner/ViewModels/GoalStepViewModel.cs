﻿using Shy.Steps;

namespace Planner.Models
{
    public class GoalStepViewModel : StepViewModelBase<GoalStep>
    {
        public GoalStepViewModel(PlanViewModel plan, GoalStep step) : base(plan, step)
        {

        }

        public GoalSize Size => Step.Size;

        public int Id => Step.Id;

        public void GoToPage()
        {
            NavigationManager.NavigateTo($"/projects/{Plan.ProjectId}/goals/{Id}");
        }
    }
}
