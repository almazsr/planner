﻿using Planner.Models;
using Shy.Steps;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Planner.ViewModels
{
    public class ProjectViewModel : BaseViewModel
    {
        private readonly Project _project;
        private readonly Plan1 _plan;

        public int Id => _project.Id;

        public Project Model => _project;

        public IEnumerable<DesireViewModel> GetDesires()
        {
            var desires = _plan.OfType<DesiresStep>().First();
            return _plan.EnumerateChildren(desires).OfType<DesireStep>().Select(s => new DesireViewModel(this, s));
        }

        public PlanViewModel GetRoutine()
        {
            var routine = _plan.OfType<RoutineStep>().First();
            return new PlanViewModel(this, routine);
        }

        internal IStep GetStepModel(int id)
        {
            return _plan.First(s => s.Id == id);
        }

        public string Name => _project.Name;

        public ProjectViewModel(Project project)
        {
            _project = project;
            _plan = _project.Plan;
        }

        public void GoToDesireNewPage()
        {
            NavigationManager.NavigateTo($"/projects/{Id}/desires/new");
        }

        internal void UpdateSubPlan(IStep parentStep, Plan1 newPlan)
        {
            var originalPlan = Model.Plan;
            var originalChildren = originalPlan.EnumerateChildren(parentStep).ToArray();
            var parentStepIndex = originalPlan.IndexOf(parentStep);
            var count = originalChildren.Length;

            originalPlan.SafeReplaceRangeAt(parentStepIndex + 1, count, newPlan);
        }

        public void InsertNewDesire(DesireNewPageViewModel viewModel)
        {
            var step = viewModel.Model;
            var steps = new List<IStep>(new[] { step });
            var lastDesire = _plan.OfType<DesireStep>().LastOrDefault();
            var index = lastDesire != null ? _plan.IndexOf(lastDesire) : 0;
            _plan.SafeInsertRange(index + 1, steps);

            UpdateSubPlan(step, viewModel.Plan.Model);
        }

        public static ProjectViewModel GetOrCreate(int projectId)
        {
            var project = ProjectProvider.GetOrReadProject();
            Services.LabelsCache.Fill(project);
            return new ProjectViewModel(project);
        }

        public void Save()
        {
            _project.Save();
        }
    }
}
