﻿using Microsoft.AspNetCore.Components;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Planner.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public NavigationManager NavigationManager { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged([CallerMemberName] string property = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public void SetNavigationManager(NavigationManager navigationManager)
        {
            NavigationManager = navigationManager;
        }
    }
}
