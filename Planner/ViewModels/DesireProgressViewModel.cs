﻿using Planner.Models;
using Shy.Steps;
using System;
using System.Collections.Generic;

namespace Planner.ViewModels
{
    public class DesireProgressViewModel : BaseViewModel
    {
        private Progress _progress;
        private readonly IList<IStep> _steps;

        public Progress Model => _progress;

        public DesireProgressViewModel(IList<IStep> steps)
        {
            _steps = steps;
            Refresh();
        }

        public void Refresh()
        {
            _progress = Services.ProgressCalculator.CalculateProgress(_steps);
        }

        public int Percent => (int)_progress.Average * 100;

        public Estimate TotalEstimate => _progress.Estimate;

        public Effort DoneEffort => _progress.Done;

        public Estimate LeftEstimate => TotalEstimate - DoneEffort;

        public bool IsUnknown => Model == Progress.Unknown;
    }
}
