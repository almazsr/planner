﻿using Shy.Steps;
using System;

namespace Planner.Models
{
    public class WhenStepEditViewModel : StepEditViewModelBase<WhenBlock>
    {
        public WhenStepEditViewModel(PlanViewModel plan, WhenBlock step) : 
            base(plan, step)
        {

        }

        public DateTimeOffset? Date
        {
            get => Step.Date;
            set => Step.Date = value;
        }
    }
}
