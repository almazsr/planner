﻿namespace Planner.Models
{
    public interface IPlanItemViewModel
    {
        int IndentLevel { get; }

        void Click();
    }
}
