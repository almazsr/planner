﻿using Shy.Steps;
using System.Linq;

namespace Planner.Models
{
    public class TaskStepViewModel : StepViewModelBase<TaskStep>
    {
        public TaskStepViewModel(PlanViewModel plan, TaskStep model) : base(plan, model)
        {

        }

        public TaskSize Size => Step.Size;

        public bool IsInProgress => Step.IsInProgress;

        public string FirstVerb => Body.Split(' ').Take(1).First();

        public string Subject => string.Join(' ', Body.Split(' ').Skip(1));
    }
}
