﻿using Shy.Steps;
using System;

namespace Planner.Models
{
    public class WhenStepViewModel : StepViewModelBase<WhenBlock>
    {
        public WhenStepViewModel(PlanViewModel plan, WhenBlock step) : base(plan, step)
        {

        }

        public TimeSpan? TimeLeft => Step.Date != null ? DateTimeOffset.Now - Step.Date : null;
    }
}
