﻿using Shy.Steps;

namespace Planner.Models
{
    public abstract class StepViewModelBase : PlanItemViewModel, IStepViewModel
    {
        private readonly IStep _step;

        protected StepViewModelBase(PlanViewModel plan, IStep model) : base(plan, model.IndentLevel)
        {
            _step = model;
        }

        public virtual IStep Model => _step;

        public string Body => _step.Body;
        public string Label => _step.Label;
        public bool IsDone => _step.IsDone;
    }

    public abstract class StepViewModelBase<TStep> : StepViewModelBase where TStep : IStep
    {
        public new TStep Model => (TStep) base.Model;

        protected TStep Step => (TStep) base.Model;

        protected StepViewModelBase(PlanViewModel plan, TStep model) : base(plan, model)
        {
        }

        public void Finish()
        {
            Step.IsDone = true;
        }

        public override void Click()
        {
            Edit();
        }

        public void Edit()
        {
            Plan.EndCreateOrEditStep();
            Plan.BeginEditStep(this);
        }
    }
}
