﻿using Shy.Steps;
using System;

namespace Planner.Models
{
    public abstract class StepEditViewModelBase : StepViewModelBase, IStepEditViewModel
    {
        private readonly IStep _step;

        protected StepEditViewModelBase(PlanViewModel plan, IStep model) : base(plan, model)
        {
            _step = model;
        }

        public override IStep Model => _step;

        public void InsertNewStepAfter()
        {
            Plan.EndCreateStep();
            Plan.BeginCreateStep(this, 1);
            Plan.EndEditStep();
        }

        public void InsertNewStepBefore()
        {
            Plan.EndCreateStep();
            Plan.BeginCreateStep(this, -1);
            Plan.EndEditStep();
        }

        public void Save()
        {
            // TODO: Save step in storage
            Commit();
        }

        public void Finish()
        {
            _step.IsDone = true;
        }

        public void Commit()
        {
            Plan.EndEditStep();
        }

        public bool CanInsertNewStepBefore()
        {
            return true;
        }

        public bool CanInsertNewStepAfter()
        {
            return true;
        }

        public bool CanDeleteStep()
        {
            return !HasChidlren();
        }

        public void DeleteStep()
        {
            Plan.RemoveStep(this);
        }

        public void CopyAndInsertStepAfter()
        {

        }
    }

    public abstract class StepEditViewModelBase<TStep> : StepEditViewModelBase where TStep : IStep
    {
        public new TStep Model => (TStep)base.Model;

        protected TStep Step => (TStep)base.Model;

        protected StepEditViewModelBase(PlanViewModel plan, TStep model) : base(plan, model)
        {
        }
    }
}
