﻿using Shy.Steps;
using System;

namespace Planner.Models
{
    public class TaskSizeConverter : ITaskSizeConverter
    {
        public Estimate TaskSizeToEstimate(TaskSize taskSize)
        {
            if (taskSize == TaskSize.Small)
            {
                return Estimate.Range(Effort.FromHours(0.5), Effort.FromDays(1));
            }
            if (taskSize == TaskSize.Normal)
            {
                return Estimate.Range(Effort.FromDays(0.5), Effort.FromWeeks(1));
            }
            if (taskSize == null)
            {
                return Estimate.Unknown;
            }
            throw new ArgumentOutOfRangeException(nameof(taskSize));
        }
        public TaskSize EstimateToTaskSize(Estimate estimate)
        {
            if (estimate.Min > Effort.FromWeeks(1))
            {
                throw new ArgumentOutOfRangeException(nameof(estimate));
            }
            if (estimate.Min > Effort.FromDays(1))
            {
                return TaskSize.Normal;
            }
            return TaskSize.Small;
        }
    }
}
