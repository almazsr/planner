﻿using Planner.Models;
using Shy.Steps;

namespace Planner.ViewModels
{
    public class DesireNewPageViewModel : DesireEditPageViewModel
    {
        public DesireNewPageViewModel(ProjectViewModel project) : base(project, new DesireStep())
        {
        }

        public override void Save()
        {
            Project.InsertNewDesire(this);
            base.Save();
        }
    }
}
