﻿using Shy.Steps;

namespace Planner.Models
{
    public class GoToStepViewModel : StepViewModelBase<GoToStep>
    {
        public GoToStepViewModel(PlanViewModel plan, GoToStep step) : base(plan, step)
        {

        }

        public string DestinationLabel => Step.DestinationLabel;
    }
}
