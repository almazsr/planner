﻿using Shy.Steps;

namespace Planner.Models
{
    public class DefaultStepViewModel : StepViewModelBase<IStep>
    {
        public DefaultStepViewModel(PlanViewModel plan, IStep model) : base(plan, model)
        {
        }
    }
}
