﻿using Shy.Steps;

namespace Planner.Models
{
    public class StatementStepViewModel : StepViewModelBase<IStatementStep>
    {
        public StatementStepViewModel(PlanViewModel plan, IStatementStep model) : base(plan, model)
        {
        }
    }
}
