﻿namespace Planner.Models
{
    public interface IStepEditViewModel : IStepViewModel
    {
        void Commit();
        void Finish();
        bool CanInsertNewStepBefore();
        void InsertNewStepBefore();
        void DeleteStep();
        bool CanDeleteStep();
        void CopyAndInsertStepAfter();
        void InsertNewStepAfter();
        bool CanInsertNewStepAfter();
        void Save();
    }
}