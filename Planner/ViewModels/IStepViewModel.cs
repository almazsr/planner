﻿using Shy.Steps;

namespace Planner.Models
{
    public interface IStepViewModel : IPlanItemViewModel
    { 
        IStep Model { get; }

        string Body { get; }

        string Label { get; }

        bool IsDone { get; }
    }
}
