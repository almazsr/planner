﻿using Shy.Steps;

namespace Planner.Models
{
    public class ElseStepViewModel : StepViewModelBase<ElseStep>
    {
        public ElseStepViewModel(PlanViewModel plan, ElseStep model) : base(plan, model)
        {

        }

        public string Command => Step.Command;
    }
}
