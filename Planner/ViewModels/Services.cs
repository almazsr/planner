﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using Planner.ViewModels;
using Shy.Steps;
using System;
using System.Linq;

namespace Planner.Models
{
    public class Services
    {
        public Services(IStepParser stepParser,
            ITaskSizeConverter taskSizeConverter, 
            IProgressCalculator progressCalculator)
        {
            StepParser = stepParser;
            TaskSizeConverter = taskSizeConverter;
            ProgressCalculator = progressCalculator;
            LabelsCache = new LabelsCache();
        }
        public static ITaskSizeConverter TaskSizeConverter { get; private set; }

        public static IStepParser StepParser { get; private set; }

        public static IProgressCalculator ProgressCalculator { get; private set; }

        public static LabelsCache LabelsCache { get; private set; }
    }
}
