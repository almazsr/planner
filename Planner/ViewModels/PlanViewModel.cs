﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;
using Planner.ViewModels;
using Shy.Steps;

namespace Planner.Models
{
    public class PlanViewModel : BaseViewModel
    {
        public Plan1 Model => _plan;

        private readonly Plan1 _plan;

        protected ProjectViewModel Project { get; }

        private readonly IStep _parentStep;
        protected ObservableRangeCollection<PlanItemViewModel> _items;

        public IReadOnlyList<PlanItemViewModel> Items => _items;

        public int ProjectId => Project.Id;

        public PlanViewModel(ProjectViewModel project, IStep parentStep)
        {
            Project = project;
            _parentStep = parentStep;

            var plan = Project.Model.Plan;

            _plan = parentStep != null ? new Plan1(plan.EnumerateChildren(parentStep)) : plan;

            _items = new ObservableRangeCollection<PlanItemViewModel>(ToPlanItemViewModels(_plan));

            _items.CollectionChanged += OnItemsChanged;
        }

        private void OnItemsChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdateModel(e);

            Debug.WriteLine($"{e.Action}: Old: {e.OldItems?[0]?.GetType()}; New: {e.NewItems?[0]?.GetType()}");
            RaisePropertyChanged(nameof(Items));
        }

        private void UpdateModel(NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    _plan.SafeInsertRange(e.NewStartingIndex, e.NewItems.OfType<IStepViewModel>().Select(s => s.Model));
                    break;
                case NotifyCollectionChangedAction.Remove:
                    _plan.SafeRemoveRangeAt(e.OldStartingIndex, e.OldItems.Count);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    _plan.SafeReplaceRangeAt(e.NewStartingIndex, e.NewItems.OfType<IStepViewModel>().Select(s => s.Model));
                    break;
            }

            Project.UpdateSubPlan(_parentStep, _plan);

            ModelChanged?.Invoke(this, EventArgs.Empty);

            Project.Save();
        }

        protected virtual void OnModelChanged()
        {
            ModelChanged?.Invoke(this, EventArgs.Empty);
        }

        internal IEnumerable<PlanItemViewModel> EnumerateChildren(PlanItemViewModel item)
        {
            var index = _items.IndexOf(item);
            return _items.Skip(index + 1).TakeWhile(i => i.IndentLevel > item.IndentLevel);
        }

        public void ToggleChidlrenVisibility(PlanItemViewModel item)
        {
            var children = EnumerateChildren(item);
            foreach (var child in children)
            {
                child.IsVisible = !item.IsCollapsed;
            }
            RaisePropertyChanged(nameof(Items));
        }

        public void InsertNewStep(NewStepViewModel viewModel, IStep step)
        {
            var steps = new List<IStep>(new[] { step });

            if (step is IfStep)
            {
                var indentLevel = step.IndentLevel + 1;
                steps.Add(new ThenStep { IndentLevel = indentLevel });
                steps.Add(new ElseStep { IndentLevel = indentLevel });
            }
            var planItems = ToPlanItemViewModels(steps);

            _items.SafeReplaceRange(viewModel, planItems);
        }

        public void Replace(PlanItemViewModel oldPlanItem, PlanItemViewModel planItem)
        {
            _items.SafeReplace(oldPlanItem, planItem);
        }

        public virtual PlanItemViewModel ToPlanItemViewModel(IStep step)
        {
            switch (step)
            {
                case TaskStep taskStep:
                    return new TaskStepViewModel(this, taskStep);
                case GoalStep goalStep:
                    return new GoalStepViewModel(this, goalStep);
                case WhenBlock whenStep:
                    return new WhenStepViewModel(this, whenStep);
                case GoToStep gotoStep:
                    return new GoToStepViewModel(this, gotoStep);
                case AfterStep afterStep:
                    return new AfterStepViewModel(this, afterStep);
                case BlockStep blockStep:
                    return new BlockStepViewModel(this, blockStep);
                case RoutineStep routineStep:
                    return new RoutineViewModel(this, routineStep, 0);
                default:
                    return new DefaultStepViewModel(this, step);
            }
        }
        public StepEditViewModelBase ToStepEditViewModel(IStep step)
        {
            switch (step)
            {
                case TaskStep taskStep:
                    return new TaskStepEditViewModel(this, taskStep);
                case WhenBlock whenStep:
                    return new WhenStepEditViewModel(this, whenStep);
                default:
                    return new DefaultStepEditViewModel(this, step);
            }
        }

        public IEnumerable<PlanItemViewModel> ToPlanItemViewModels(IList<IStep> steps)
        {
            if (!steps.Any())
            {
                yield return new NewStepViewModel(this, _parentStep.IndentLevel + 1);
            }
            for (var i = 0; i < steps.Count; i++)
            {
                var step = steps[i];
                yield return ToPlanItemViewModel(step);
                if (step is IHasBlock)
                {
                    var hasEmptyBlock = i == steps.Count - 1 || steps[i + 1].IndentLevel <= step.IndentLevel;
                    if (hasEmptyBlock)
                    {
                        yield return new FirstStepViewModel(this, step.IndentLevel + 1);
                    }
                }
            }
        }

        public void BeginCreateStep(FirstStepViewModel item)
        {
            NewStep = item;
        }

        public void BeginCreateStep(PlanItemViewModel item, int offset)
        {
            var itemIndex = _items.IndexOf(item);
            var index = itemIndex + offset;
            if (index >= 0 && index < _items.Count && _items[index].IndentLevel != item.IndentLevel)
            {
                index = itemIndex;
            }
            NewStep = new NewStepViewModel(this, item.IndentLevel);
            _items.SafeInsert(index, NewStep);
        }

        internal void RemoveItem(PlanItemViewModel item)
        {
            _items.Remove(item);
        }

        public void EndCreateStep()
        {
            if (NewStep != null)
            {
                NewStep.Commit();
                NewStep = null;
            }
        }

        public void EndCreateOrEditStep()
        {
            EndEditStep();
            EndCreateStep();
        }

        public void EndEditStep()
        {
            if (EditingStep != null)
            {
                var viewModel = ToPlanItemViewModel(EditingStep.Model);
                _items.SafeReplace(EditingStep, viewModel);
                EditingStep = null;
            }
        }

        public void BeginEditStep(StepViewModelBase arg)
        {
            EditingStep = ToStepEditViewModel(arg.Model);
            _items.SafeReplace(arg, EditingStep);
        }

        public void RemoveStep(StepViewModelBase step)
        {
            _items.Remove(step);
        }

        public StepViewModelBase EditingStep { get; private set; }

        public NewStepViewModel NewStep { get; private set; }

        public event EventHandler ModelChanged;
    }
}
