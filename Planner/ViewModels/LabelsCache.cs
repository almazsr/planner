﻿using Shy.Steps;
using System.Collections.Generic;
using System.Linq;

namespace Planner.Models
{
    public class LabelsCache
    {
        private Dictionary<string, IStep> _labelBySteps;

        public void Fill(Project project)
        {
            _labelBySteps = project.Plan.Where(s => s.Label != null).ToDictionary(s => s.Label, s => s);
        }

        public string[] GetLabels()
        {
            return _labelBySteps.Keys.ToArray();
        }

        public IStep GetStepByLabel(string label)
        {
            return _labelBySteps[label];
        }

        public bool ContainsLabel(string label)
        {
            return _labelBySteps.ContainsKey(label);
        }

        public void Add(string label, IStep step)
        {
            _labelBySteps.Add(label, step);
        }
    }
}
