﻿using Shy.Steps;

namespace Planner.Models
{
    public class RoutineViewModel : PlanItemViewModel
    {
        public RoutineViewModel(PlanViewModel plan, RoutineStep step, int indentLevel) : base(plan, indentLevel)
        {
            Model = step;
        }

        public RoutineStep Model { get; }
    }
}
