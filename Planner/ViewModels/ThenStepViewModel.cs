﻿using Shy.Steps;

namespace Planner.Models
{
    public class ThenStepViewModel : StepViewModelBase<ThenStep>
    {
        public ThenStepViewModel(PlanViewModel plan, ThenStep model) : base(plan, model)
        {

        }

        public string Command => Step.Command;
    }
}
