﻿using Planner.ViewModels;
using Shy.Steps;
using System;

namespace Planner.Models
{
    public class ComplexStepEditViewModelBase<TStep> : BaseViewModel where TStep : IStep
    {
        protected TStep Step { get; }

        public Progress GetProgress()
        {
            return Services.ProgressCalculator.CalculateProgress(Plan.Model);
        }

        public PlanViewModel Plan { get; private set; }
        protected ProjectViewModel Project { get; }

        public TStep Model => Step;

        public int Id { get; set; }

        public ComplexStepEditViewModelBase(ProjectViewModel project, TStep step)
        {
            Project = project;
            Step = step;
            Plan = new PlanViewModel(Project, step);
        }

        public virtual void Save()
        {
            Project.Save();
            NavigationManager.NavigateTo("/");
        }
    }
}
