﻿using OfficeOpenXml;
using System;
using System.Globalization;

namespace ScheduleGenerator
{
    public class TaskStatusRecord
    {
        public const int ColumnOfLast = 68;

        public const int ColumnOfWorkScope = 1;
        public const int ColumnOfTicketId = 2;
        public const int ColumnOfName = 3;
        public const int ColumnOfDate = 4;
        public const int ColumnOfPersonInCharge = 5;
        public const int ColumnOfStatus = 6;
        public const int ColumnOfStartDate = 7;
        public const int ColumnOfFinishDate = 8;
        public const int ColumnOfDelayInDays = 9;
        public const int ColumnOfDelayReason = 10;
        public const int ColumnOfRemedyAction = 11;

        public const int ColumnOfSSEstimatedEffortInHours = 13;
        public const int ColumnOfSSEstimatedPages = 14;
        public const int ColumnOfSSEstimatedReviews = 15;
        public const int ColumnOfSSStartDate = 16;
        public const int ColumnOfSSFinishDate = 17;
        public const int ColumnOfSSDelayReason = 18;
        public const int ColumnOfSSDonePages = 19;
        public const int ColumnOfSSImplementationProgress = 20;
        public const int ColumnOfSSIsSelfReviewDone = 21;
        public const int ColumnOfSSIsHorizontalReviewDone = 22;
        public const int ColumnOfSSIsVerticalReviewDone = 23;
        public const int ColumnOfSSIsJapaneseReviewDone = 24;
        public const int ColumnOfSSReviewProgress = 25;

        public const int ColumnOfSSProgress = 27;
        public const int ColumnOfPGEstimatedEffortInHours = 28;
        public const int ColumnOfPGEstimatedKS = 29;
        public const int ColumnOfPGEstimatedMethods = 30;
        public const int ColumnOfPGEstimatedReviews = 31;
        public const int ColumnOfPGStartDate = 32;
        public const int ColumnOfPGFinishDate = 33;
        public const int ColumnOfPGDelayReason = 34;
        public const int ColumnOfPGDoneKS = 35;
        public const int ColumnOfPGKSProgress = 36;
        public const int ColumnOfPGDoneMethods = 37;
        public const int ColumnOfPGMethodsProgress = 38;
        public const int ColumnOfPGImplementationProgress = 39;
        public const int ColumnOfPGIsSelfReviewDone = 40;
        public const int ColumnOfPGIsHorizontalReviewDone = 41;
        public const int ColumnOfPGIsVerticalReviewDone = 42;
        public const int ColumnOfPGIsJapaneseReviewDone = 43;
        public const int ColumnOfPGReviewProgress = 44;
        public const int ColumnOfPGProgress = 45;

        public const int ColumnOfPTEstimatedEffortInHours = 47;
        public const int ColumnOfPTEstimatedTestItems = 48;
        public const int ColumnOfPTEstimatedReviews = 49;
        public const int ColumnOfPTStartDate = 50;
        public const int ColumnOfPTFinishDate = 51;
        public const int ColumnOfPTDelayReason = 52;
        public const int ColumnOfPTDoneTestItems = 53;
        public const int ColumnOfPTTestItemsProgress = 54;
        public const int ColumnOfPTCoveredMethods = 55;
        public const int ColumnOfPTCoveredMethodsProgress = 56;
        public const int ColumnOfPTImplementationProgress = 57;
        public const int ColumnOfPTIsSelfReviewDone = 58;
        public const int ColumnOfPTIsHorizontalReviewDone = 59;
        public const int ColumnOfPTIsVerticalReviewDone = 60;
        public const int ColumnOfPTIsJapaneseReviewDone = 61;
        public const int ColumnOfPTReviewProgress = 62;
        public const int ColumnOfPTProgress = 63;

        public const int ColumnOfMRCreated = 65;
        public const int ColumnOfMRMerged = 66;
        public const int ColumnOfMergeProgress = 67;
        public const int ColumnOfTotalProgress = 68;

        public int RowIndex { get; set; }

        public string WorkScope { get; set; }
        public int TicketId { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public string PersonInCharge { get; set; }

        public string Status { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }

        public int DelayInDays { get; set; }

        public string DelayReason { get; set; }

        public string RemedyAction { get; set; }

        public int SSEstimatedEffortInHours { get; set; }

        public int SSEstimatedPages { get; set; }

        public int SSEstimatedReviews { get; set; }

        public DateTime SSStartDate { get; set; }
        public DateTime SSFinishDate { get; set; }

        public string SSDelayReason { get; set; }

        public int SSDonePages { get; set; }

        public double SSImplementationProgress { get; set; }

        public bool SSIsSelfReviewDone { get; set; }
        public bool SSIsHorizontalReviewDone { get; set; }
        public bool SSIsVerticalReviewDone { get; set; }
        public bool SSIsJapaneseReviewDone { get; set; }

        public double SSReviewProgress { get; set; }

        public double SSProgress { get; set; }

        public int PGEstimatedEffortInHours { get; set; }

        public double PGEstimatedKS { get; set; }
        public int PGEstimatedMethods { get; set; }
        public int PGEstimatedReviews { get; set; }

        public DateTime PGStartDate { get; set; }
        public DateTime PGFinishDate { get; set; }

        public string PGDelayReason { get; set; }

        public double PGDoneKS { get; set; }
        public double PGKSProgress { get; set; }
        public int PGDoneMethods { get; set; }
        public double PGMethodsProgress { get; set; }

        public double PGImplementationProgress { get; set; }

        public bool PGIsSelfReviewDone { get; set; }
        public bool PGIsHorizontalReviewDone { get; set; }
        public bool PGIsVerticalReviewDone { get; set; }
        public bool PGIsJapaneseReviewDone { get; set; }

        public double PGReviewProgress { get; set; }

        public double PGProgress { get; set; }

        public int PTEstimatedEffortInHours { get; set; }

        public int PTEstimatedTestItems { get; set; }
        public int PTEstimatedReviews { get; set; }

        public DateTime PTStartDate { get; set; }
        public DateTime PTFinishDate { get; set; }

        public string PTDelayReason { get; set; }

        public int PTDoneTestItems { get; set; }
        public double PTTestItemsProgress { get; set; }
        public int PTCoveredMethods { get; set; }
        public double PTCoveredMethodsProgress { get; set; }
        public double PTImplementationProgress { get; set; }

        public bool PTIsSelfReviewDone { get; set; }
        public bool PTIsHorizontalReviewDone { get; set; }
        public bool PTIsVerticalReviewDone { get; set; }
        public bool PTIsJapaneseReviewDone { get; set; }

        public double PTReviewProgress { get; set; }

        public double PTProgress { get; set; }

        public int MRCreated { get; set; }

        public int MRMerged { get; set; }

        public double MergeProgress { get; set; }

        public double TotalProgress { get; set; }

        public static TaskStatusRecord Parse(ExcelWorksheet worksheet, TaskStatusRecord parentTaskStatusRecord, int row, DateTime date)
        {
            var result = new TaskStatusRecord { RowIndex = row };
            result.Date = date;

            DateTime ParseDate(object str)
            {
                if (str != null && DateTime.TryParse(str.ToString(), out DateTime dateRes))
                {
                    return dateRes;
                }
                return default(DateTime);
            }
             
            int ParseInt(object str)
            {
                if (str != null && int.TryParse(str.ToString(), out int intRes))
                {
                    return intRes;
                }
                return 0;
            }

            double ParseDouble(object str)
            {
                if (str != null && double.TryParse(str.ToString(), out double dblRes))
                {
                    return dblRes;
                }
                return 0;
            }

            bool ParseBool(object str)
            {
                if (str != null && int.TryParse(str.ToString(), out int blRes))
                {
                    return blRes == 1;
                }
                return false;
            }

            var name = worksheet.Cells[row, ColumnOfName].Value?.ToString();

            if (!string.IsNullOrEmpty(name))
            {
                result.WorkScope = worksheet.Cells[row, ColumnOfWorkScope].Value?.ToString();
                result.TicketId = ParseInt(worksheet.Cells[row, ColumnOfTicketId].Value);
                result.Name = name;
            }
            else
            {
                result.Name = parentTaskStatusRecord.Name;
                result.TicketId = parentTaskStatusRecord.TicketId;
                result.WorkScope = parentTaskStatusRecord.WorkScope;
                result.PersonInCharge = worksheet.Cells[row, ColumnOfPersonInCharge].Value?.ToString();
                result.Status = worksheet.Cells[row, ColumnOfStatus].Value?.ToString();
                result.DelayReason = worksheet.Cells[row, ColumnOfDelayReason].Value?.ToString();
                result.RemedyAction = worksheet.Cells[row, ColumnOfRemedyAction].Value?.ToString();
                result.SSDelayReason = worksheet.Cells[row, ColumnOfSSDelayReason].Value?.ToString();
                result.PGDelayReason = worksheet.Cells[row, ColumnOfPGDelayReason].Value?.ToString();
                result.PTDelayReason = worksheet.Cells[row, ColumnOfPTDelayReason].Value?.ToString();
            }

            result.Date = ParseDate(worksheet.Cells[row, ColumnOfDate].Value);
            result.DelayInDays = ParseInt(worksheet.Cells[row, ColumnOfDelayInDays].Value);
            result.StartDate = ParseDate(worksheet.Cells[row, ColumnOfStartDate].Value);
            result.FinishDate = ParseDate(worksheet.Cells[row, ColumnOfFinishDate].Value);
            result.SSEstimatedEffortInHours = ParseInt(worksheet.Cells[row, ColumnOfSSEstimatedEffortInHours].Value);
            result.SSEstimatedPages = ParseInt(worksheet.Cells[row, ColumnOfSSEstimatedPages].Value);
            result.SSEstimatedReviews = ParseInt(worksheet.Cells[row, ColumnOfSSEstimatedReviews].Value);
            result.SSStartDate = ParseDate(worksheet.Cells[row, ColumnOfSSStartDate].Value);
            result.SSFinishDate = ParseDate(worksheet.Cells[row, ColumnOfSSFinishDate].Value);
            result.SSDonePages = ParseInt(worksheet.Cells[row, ColumnOfSSDonePages].Value);
            result.SSImplementationProgress = ParseDouble(worksheet.Cells[row, ColumnOfSSImplementationProgress].Value);
            result.SSIsSelfReviewDone = ParseBool(worksheet.Cells[row, ColumnOfSSIsSelfReviewDone].Value);
            result.SSIsHorizontalReviewDone = ParseBool(worksheet.Cells[row, ColumnOfSSIsHorizontalReviewDone].Value);
            result.SSIsVerticalReviewDone = ParseBool(worksheet.Cells[row, ColumnOfSSIsVerticalReviewDone].Value);
            result.SSIsJapaneseReviewDone = ParseBool(worksheet.Cells[row, ColumnOfSSIsJapaneseReviewDone].Value);
            result.SSReviewProgress = ParseDouble(worksheet.Cells[row, ColumnOfSSReviewProgress].Value);
            result.SSProgress = ParseDouble(worksheet.Cells[row, ColumnOfSSProgress].Value);
            result.PGEstimatedEffortInHours = ParseInt(worksheet.Cells[row, ColumnOfPGEstimatedEffortInHours].Value);
            result.PGEstimatedKS = ParseDouble(worksheet.Cells[row, ColumnOfPGEstimatedKS].Value);
            result.PGEstimatedMethods = ParseInt(worksheet.Cells[row, ColumnOfPGEstimatedMethods].Value);
            result.PGEstimatedReviews = ParseInt(worksheet.Cells[row, ColumnOfPGEstimatedReviews].Value);
            result.PGStartDate = ParseDate(worksheet.Cells[row, ColumnOfPGStartDate].Value);
            result.PGFinishDate = ParseDate(worksheet.Cells[row, ColumnOfPGFinishDate].Value);
            result.PGDoneKS = ParseDouble(worksheet.Cells[row, ColumnOfPGDoneKS].Value);
            result.PGKSProgress = ParseDouble(worksheet.Cells[row, ColumnOfPGKSProgress].Value);
            result.PGDoneMethods = ParseInt(worksheet.Cells[row, ColumnOfPGDoneMethods].Value);
            result.PGMethodsProgress = ParseDouble(worksheet.Cells[row, ColumnOfPGMethodsProgress].Value);
            result.PGImplementationProgress = ParseDouble(worksheet.Cells[row, ColumnOfPGImplementationProgress].Value);
            result.PGIsSelfReviewDone = ParseBool(worksheet.Cells[row, ColumnOfPGIsSelfReviewDone].Value);
            result.PGIsHorizontalReviewDone = ParseBool(worksheet.Cells[row, ColumnOfPGIsHorizontalReviewDone].Value);
            result.PGIsVerticalReviewDone = ParseBool(worksheet.Cells[row, ColumnOfPGIsVerticalReviewDone].Value);
            result.PGIsJapaneseReviewDone = ParseBool(worksheet.Cells[row, ColumnOfPGIsJapaneseReviewDone].Value);
            result.PGReviewProgress = ParseDouble(worksheet.Cells[row, ColumnOfPGReviewProgress].Value);
            result.PGProgress = ParseDouble(worksheet.Cells[row, ColumnOfPGProgress].Value);
            result.PTEstimatedEffortInHours = ParseInt(worksheet.Cells[row, ColumnOfPTEstimatedEffortInHours].Value);
            result.PTEstimatedTestItems = ParseInt(worksheet.Cells[row, ColumnOfPTEstimatedTestItems].Value);
            result.PTEstimatedReviews = ParseInt(worksheet.Cells[row, ColumnOfPTEstimatedReviews].Value);
            result.PTStartDate = ParseDate(worksheet.Cells[row, ColumnOfPTStartDate].Value);
            result.PTFinishDate = ParseDate(worksheet.Cells[row, ColumnOfPTFinishDate].Value);
            result.PTDoneTestItems = ParseInt(worksheet.Cells[row, ColumnOfPTDoneTestItems].Value);
            result.PTTestItemsProgress = ParseDouble(worksheet.Cells[row, ColumnOfPTTestItemsProgress].Value);
            result.PTCoveredMethods = ParseInt(worksheet.Cells[row, ColumnOfPTCoveredMethods].Value);
            result.PTCoveredMethodsProgress = ParseDouble(worksheet.Cells[row, ColumnOfPTCoveredMethodsProgress].Value);
            result.PTImplementationProgress = ParseDouble(worksheet.Cells[row, ColumnOfPTImplementationProgress].Value);
            result.PTIsSelfReviewDone = ParseBool(worksheet.Cells[row, ColumnOfPTIsSelfReviewDone].Value);
            result.PTIsHorizontalReviewDone = ParseBool(worksheet.Cells[row, ColumnOfPTIsHorizontalReviewDone].Value);
            result.PTIsVerticalReviewDone = ParseBool(worksheet.Cells[row, ColumnOfPTIsVerticalReviewDone].Value);
            result.PTIsJapaneseReviewDone = ParseBool(worksheet.Cells[row, ColumnOfPTIsJapaneseReviewDone].Value);
            result.PTReviewProgress = ParseDouble(worksheet.Cells[row, ColumnOfPTReviewProgress].Value);
            result.PTProgress = ParseDouble(worksheet.Cells[row, ColumnOfPTProgress].Value);
            result.MRCreated = ParseInt(worksheet.Cells[row, ColumnOfMRCreated].Value);
            result.MRMerged = ParseInt(worksheet.Cells[row, ColumnOfMRMerged].Value);
            result.MergeProgress = ParseDouble(worksheet.Cells[row, ColumnOfMergeProgress].Value);
            result.TotalProgress = ParseDouble(worksheet.Cells[row, ColumnOfTotalProgress].Value);

            return result;
        }
    }
}
