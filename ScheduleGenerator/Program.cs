﻿
using OfficeOpenXml;
using Shy.Steps;
using Shy.Steps.Validation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleGenerator
{
    class Program
    {
        static Plan1 Parse(string filePath)
        {
            var planParser = new PlanParser(new StepParser(new TaskNameValidator()));
            using (var textReader = new StreamReader(File.OpenRead(filePath)))
            {
                return planParser.ParsePlanAsync(textReader).Result.Data;
            }
        }

        static bool IsRowEmpty(ExcelWorksheet worksheet, int rowIndex)
        {
            var range = worksheet.Cells[rowIndex, 1, rowIndex, TaskStatusRecord.ColumnOfLast];
            foreach (var cell in range)
            {
                if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                {
                    return false;
                }
            }
            return true;
        }

        static void Main(string[] args)
        {
            const int firstRow = 5;
            using (var package = new ExcelPackage(new FileInfo(@"C:\Users\almaz\Downloads\Status table CL_BC (19.02.2029).xlsx")))
            {
                var workSheet = package.Workbook.Worksheets[1];                
                var date = args.Length > 1 ? 
                    DateTime.ParseExact(args[1], "dd /MM/yy", CultureInfo.InvariantCulture) : 
                    DateTime.Now.Date;

                var row = firstRow;

                var records = new List<TaskStatusRecord>();

                TaskStatusRecord lastRecord = null;

                while (!IsRowEmpty(workSheet, row))
                {
                    var record = TaskStatusRecord.Parse(workSheet, lastRecord, row, date);
                    if (lastRecord == null || record.Name != lastRecord.Name)
                    {
                        lastRecord = record;
                    }
                    records.Add(record);
                    row++;
                }
                var KS = records.GroupBy(r => r.Name).Select(x => x.Last().PGEstimatedKS).Sum();
            }
        }
    }
}
